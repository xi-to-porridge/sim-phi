import os
from PIL import Image, ImageFilter, ImageEnhance
import core
import pygame
import sys
import time
import data
import element
import zipfile

pygame.init()

# # JSON_PATH = "resources/61895176/61895176.json"
# ID = "61895176"
# # ID = "56769032"
# JSON_PATH = f"resources/{ID}/{ID}.json"
# # JSON_PATH = "resources/81907165/81907165.json"
# pygame.mixer.init()
# pygame.mixer.music.load(f"resources/{ID}/{ID}.mp3")

# ZIP_PATH = "./downloaded_beatmap/Astaroth IN Lv.15.zip"
#
# for file in os.listdir("./cache/unzipped"):
#     os.remove(f"./cache/unzipped/{file}")
#
# zip_file = zipfile.ZipFile(ZIP_PATH)
# for file in zip_file.namelist():
#     zip_file.extract(file, "./cache/unzipped")

data.load_zip("../downloaded_beatmap/Phigros Theme(offset=50ms).zip")

# core.IMAGE = "./cache/Ark/24852994.jpg"
# core.SONG = "./cache/Ark/24852994.ogg"
pygame.mixer.init()
pygame.mixer.music.load(core.SONG)

pil_blurred = Image.open(core.IMAGE).filter(ImageFilter.GaussianBlur(radius=30))
brightEnhancer = ImageEnhance.Brightness(pil_blurred)
img = brightEnhancer.enhance(0.5)
img.save("./cache/bg_b_b.png")
# convert it back to a pygame surface
background = pygame.transform.smoothscale(pygame.image.load("./cache/bg_b_b.png"),
                                          (core.WIDTH, core.HEIGHT))

evalPainter = element.EvalPainter()

screen = pygame.display.set_mode((core.WIDTH, core.HEIGHT))
surface = pygame.Surface((core.WIDTH, core.HEIGHT), pygame.SRCALPHA)

font40 = pygame.font.Font("./resources/cmdysj.ttf", 40)
font30 = pygame.font.Font("./resources/cmdysj.ttf", 30)
font25 = pygame.font.Font("./resources/cmdysj.ttf", 25)
font20 = pygame.font.Font("./resources/cmdysj.ttf", 20)


skip = 0
beat = core.BeatObject.get_value(skip/60)


for jl in core.judge_line_list:

    note_bin = []
    for note in jl.not_holds:
        if note.at < beat:
            note_bin.append(note)
        else:
            break
    for note in note_bin:
        jl.not_holds.remove(note)
        if note in jl.above1:
            jl.above1.remove(note)
        else:
            jl.above2.remove(note)

    note_bin = []
    for note in jl.holds:
        if note.end < beat:
            note_bin.append(note)
        if note.at <= beat and (time.time() - note.last_eval_time >= 0.2):
            note.last_eval_time = time.time()

    for note in note_bin:
        jl.holds.remove(note)
        if note in jl.above1:
            jl.above1.remove(note)
        else:
            jl.above2.remove(note)

note_num = 0

core.OFFSET -= 175

clock = pygame.time.Clock()

name_text = font25.render("| " + core.NAME, True, (255, 255, 255))
level_text = font25.render(core.LEVEL, True, (255, 255, 255))
copyright_text = font20.render("SimPhi Project - Code by xi2p", True, (200, 200, 200))
if core.NOTE_NUM == 0:
    core.NOTE_NUM = 1
    note_num = 1
pygame.mixer.music.play(start=skip)
start = time.time()
beat = core.BeatObject.get_value((time.time()-start+skip-core.OFFSET/1000)/60)

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    if beat < 0:
        beat = core.BeatObject.get_value((time.time() - start + skip - core.OFFSET / 1000) / 60)
        continue
    screen.blit(background, (0, 0))
    surface.fill((0, 0, 0, 0))

    for jl in core.judge_line_list:
        jl.blit(surface, beat)

        note_bin = []
        for note in jl.not_holds:
            if note.at < beat:
                note_bin.append(note)
            else:
                break
        for note in note_bin:
            jl.not_holds.remove(note)
            if note in jl.above1:
                jl.above1.remove(note)
            else:
                jl.above2.remove(note)
            if not note.fake:
                evalPainter.add_note(note, core.Eval.PERFECT)
                if note.id == element.Note.TAP:
                    core.TAP_SOUND.play()
                elif note.id == element.Note.DRAG:
                    core.DRAG_SOUND.play()
                elif note.id == element.Note.FLICK:
                    core.FLICK_SOUND.play()
                note_num += 1

        note_bin = []
        for note in jl.holds:
            if note.end < beat:
                note_bin.append(note)

            if note.at <= beat and (time.time() - note.last_eval_time >= 0.2):
                if not note.fake:
                    if note.last_eval_time == -1:
                        core.TAP_SOUND.play()
                    evalPainter.add_note(note, core.Eval.PERFECT)
                note.last_eval_time = time.time()

        for note in note_bin:
            jl.holds.remove(note)
            if note in jl.above1:
                jl.above1.remove(note)
            else:
                jl.above2.remove(note)
            if not note.fake:
                note_num += 1

    evalPainter.blit(surface)

    combo_text = font30.render("COMBO", True, (255, 255, 255))
    combo_num_text = font40.render(str(note_num), True, (255, 255, 255))
    score_text = font30.render(str(int(note_num/core.NOTE_NUM*1000000)).rjust(7, '0'), True, (255, 255, 255))
    fps_text = font25.render(str(int(clock.get_fps())).rjust(3, "0"), True, (255, 255, 255))
    offset_text = font20.render(f"OFFSET={core.OFFSET}", True, (255, 255, 255))

    if note_num >= 3:
        surface.blit(combo_text, (core.WIDTH/2-combo_text.get_width()/2, 40))
        surface.blit(combo_num_text, (core.WIDTH/2-combo_num_text.get_width()/2, 0))

    surface.blit(score_text, (core.WIDTH-score_text.get_width(), 0))
    surface.blit(fps_text, (0, 0))
    surface.blit(offset_text, (0, fps_text.get_height()))

    surface.blit(name_text, (5,
                             core.HEIGHT-name_text.get_height()-5))

    surface.blit(level_text, (core.WIDTH-level_text.get_width()-5,
                              core.HEIGHT-level_text.get_height()-copyright_text.get_height()-5))

    surface.blit(copyright_text, (core.WIDTH-copyright_text.get_width()-5,
                                  core.HEIGHT-copyright_text.get_height()-5))

    screen.blit(surface, (0, 0))
    pygame.display.flip()
    clock.tick(60)
    # print(clock.get_fps())
    beat = core.BeatObject.get_value((pygame.mixer.music.get_pos()/1000+skip-core.OFFSET/1000)/60)
    # beat = core.BeatObject.get_value((time.time()-start+skip-core.OFFSET/1000)/60)

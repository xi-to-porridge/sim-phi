import jpype
if __name__ == '__main__':
    """
    ①、使用jpype开启jvm
    ②、加载java类
    ③、调用java方法
    ④、关闭jvm
    """
    # ①、使用jpype开启虚拟机（在开启jvm之前要加载类路径）
    # 加载刚才打包的jar文件
    jar_path = '/org/xi2p/Server.class'
    # 获取jvm.dll 的文件路径
    jvmPath = jpype.getDefaultJVMPath()

    # 启动jvm
    jpype.startJVM(jvmPath, "-ea", "-Djava.class.path=%s" % jar_path)

    # ②、加载java类（参数是java的长类名）
    javaClass = jpype.JClass("org.xi2p.Server")

    # 实例化java对象
    javaInstance = javaClass()
    # ③、调用java方法
    javaInstance.main([""])

    # ④、关闭jvm:jvm随着python程序的退出而结束
    jpype.shutdownJVM()
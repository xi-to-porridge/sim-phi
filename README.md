# simPhi
Phigros 自制铺的播放器，兼容PEC与RPE, 甚至还支持播放malody的铺面文件夹或铺面压缩文件夹  
没有标明版本号的文件夹为正在开发的最新版本    

## 正在开发的内容  
选歌UI  

## 最新Stable版本v1.2.0  
实现了基于socket的Python客户端与Java服务端的通讯。  
通过Java本身的计算速度优势来减少SimPhi花在运算上的时间，并减少了超出屏幕边界的note不显示的问题。  
v 1.1.0 以后的版本需要在配置了Java的环境下才可以运行。  
class文件的编译环境为JDK 17。注意Java的运行路径要包含在系统的环境变量中。  

## 运行程序(v1.1.0以后)  
1. 打开程序根目录，其main.py即为程序入口。运行该文件即可。   
2. 指定要播放的铺面，请修改main.py内的代码

		autoplay.run("你的铺面.zip", skip=跳过开头n秒)  
	
	example:  
	
		autoplay.run("../downloaded_beatmap/Phigros Theme(offset=50ms).zip", skip=22)  
		# 播放 Phigros Theme(offset=50ms).zip内的铺面， 从第22秒开始播放

3. load_beatmap函数可以支持未解压的zip铺面和已解压的铺面文件夹  

## 运行程序(v1.1.0及以前)
1. 打开程序根目录，其display.py即为程序入口。运行该文件即可。   
2. 指定要播放的铺面，请修改display.py内的代码

		data.load_beatmap("你的铺面.zip")  

3. load_beatmap函数可以支持未解压的zip铺面和已解压的铺面文件夹  
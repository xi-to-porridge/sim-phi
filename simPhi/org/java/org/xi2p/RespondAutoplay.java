package org.xi2p;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import static org.xi2p.core.*;

public class RespondAutoplay extends Respond{
    @Override
    public void respond(String command) {
        String[] blocks = command.split(" ");

        if ("gc".equals(blocks[0])) {
            // get canvas
            // command format: gc beat
            double beat = Double.parseDouble(blocks[1]);
            ArrayList<Note> notes = new ArrayList<>();
            ArrayList<Note> noteBin = new ArrayList<>();
            ArrayList<Note> Eval = new ArrayList<>();
            ArrayList<Note> Audio = new ArrayList<>();

            for (Judgeline judgeline : judgeline_list) {
//                    System.out.println(1);

                judgeline.update(beat);
                notes.addAll(judgeline.visibleNotes);

                for (Note note : judgeline.notHolds) {
                    if (note.at < beat) {
                        noteBin.add(note);
                        Eval.add(note);
                        Audio.add(note);
                    }
                }

                for (Note note : noteBin) {
                    judgeline.notHolds.remove(note);
                    judgeline.above1.remove(note);
                    judgeline.above2.remove(note);
                    perfectNum++;
                    combo++;
                }
                noteBin.clear();

                for (Note note : judgeline.holds) {
                    if (note.at < beat && System.currentTimeMillis() - note.lastEvalTime > 100) {
                        Eval.add(note);
                        if (note.lastEvalTime == -1) {
                            Audio.add(note);
                        }
                        note.lastEvalTime = System.currentTimeMillis();
                    }
                    if (note.end < beat) {
                        noteBin.add(note);
                    }
                }

                for (Note note : noteBin) {
                    judgeline.holds.remove(note);
                    judgeline.above1.remove(note);
                    judgeline.above2.remove(note);
                    perfectNum++;
                    combo++;

                }
                noteBin.clear();
            }

            for (Judgeline judgeline : judgeline_list) {
                if (judgeline.point1 != null && judgeline.point2 != null) {
                    printWriter.write("%.2f %.2f %.2f %.2f %.2f|".
                            formatted(judgeline.point1[0], judgeline.point1[1],
                                    judgeline.point2[0], judgeline.point2[1],
                                    judgeline.alpha));
//                    printWriter.write("%.2f %.2f|".formatted(judgeline.x, judgeline.y));
                }

            }
            printWriter.write("$");
            printWriter.flush();

            // 标记highlight
            ArrayList<Note> _notes = new ArrayList<>(notes);
            for (int i = 0; i < _notes.size(); i++) {
                Note noteI = _notes.get(i);
                for (int j = i + 1; j < _notes.size(); j++) {
                    Note noteJ = _notes.get(j);
                    if (noteI.at == noteJ.at) {
                        noteI.highlight = true;
                        noteJ.highlight = true;
                        j--;
                        _notes.remove(noteJ);
                    }
                }
//
            }

            LinkedHashSet<String> strings = new LinkedHashSet<>();
            notes.sort(new NoteComparator<>());
            for (Note note : notes) {
                strings.add(note.blit(beat));

            }

            for (String noteString : strings) {
                printWriter.write(noteString);
            }
//                    printWriter.write("%.2f %.2f|".formatted(note.xInSurface, note.yInSurface));

            printWriter.write("$");
            printWriter.flush();

            for (Note note : Eval) {
                printWriter.write("%.2f %.2f|".formatted(note.xInSurface, note.yInSurface));
//                    printWriter.write("%.2f %.2f|".formatted(note.xInSurface, note.yInSurface));
            }

            printWriter.write("$");
            printWriter.flush();

            for (Note note : Audio) {
                printWriter.write("%d|".formatted(note.type));
//                    printWriter.write("%.2f %.2f|".formatted(note.xInSurface, note.yInSurface));
            }

            printWriter.write("$");
            printWriter.flush();

            score = (int) (1000000 * perfectNum / noteNum);
            printWriter.write("%d|%d|%d|%d|%d|%d|%d|".formatted(
                    score, perfectNum, goodNum, badNum, missNum, combo, maxCombo));
            printWriter.write("$");
            printWriter.flush();
        } else {
            System.out.println("500 INVALID COMMAND HEAD");
        }
    }
}

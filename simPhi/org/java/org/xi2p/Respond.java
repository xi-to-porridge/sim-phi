package org.xi2p;

abstract public class Respond {
    abstract public void respond(String command);
}

package org.xi2p.easing;

public class EaseInOutBack extends Func {
    private static final double c1 = 1.70158;
    private static final double c2 = c1 * 1.525;

    public EaseInOutBack(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat) {
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x < 0.5) ? (
                    (Math.pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2) : (
                    (Math.pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2)) * k + origin;
        } else {
            return target;
        }
    }
}

package org.xi2p.easing;

public class EaseInOutBounce extends Func{
    public EaseInOutBounce(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x < 0.5) ? (1 - EaseInBounce._easeOutBounce(1 - 2 * x)) / 2
                    : (1 + EaseInBounce._easeOutBounce(2 * x - 1)) / 2) * k + origin;
        }
        else{
            return target;
        }
    }
}

package org.xi2p.easing;

public class EaseInSine extends Func{
    public EaseInSine(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1 - Math.cos((x * Math.PI) / 2)) * k + origin;
        }
        else{
            return target;
        }
    }
}

package org.xi2p.easing;

public class EaseInOutSine extends Func{
    public EaseInOutSine(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return -(Math.cos(Math.PI * x) - 1) / 2 * k + origin;
        }
        else{
            return target;
        }
    }
}

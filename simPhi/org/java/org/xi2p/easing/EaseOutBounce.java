package org.xi2p.easing;

public class EaseOutBounce extends Func{
    public EaseOutBounce(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return EaseInBounce._easeOutBounce(x) * k + origin;
        }
        else{
            return target;
        }
    }
}

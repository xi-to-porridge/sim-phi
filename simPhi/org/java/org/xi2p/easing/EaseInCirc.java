package org.xi2p.easing;

public class EaseInCirc extends Func{

    public EaseInCirc(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1.0 - Math.sqrt(1 - Math.pow(x, 2))) * k + origin;
        }
        else{
            return target;
        }
    }

}

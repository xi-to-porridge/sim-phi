package org.xi2p.easing;

public class EaseOutQuint extends Func{
    public EaseOutQuint(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1 - Math.pow(1 - x, 5)) * k + origin;
        }
        else{
            return target;
        }
    }
}

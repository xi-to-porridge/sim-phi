package org.xi2p.easing;

public class EaseOutCirc extends Func{
    public EaseOutCirc(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (Math.sqrt(1 - Math.pow(x - 1, 2))) * k + origin;
        }
        else{
            return target;
        }
    }
}

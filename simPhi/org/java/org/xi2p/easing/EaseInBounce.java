package org.xi2p.easing;

public class EaseInBounce extends Func{
    public static final double n1 = 7.5625;
    public static final double d1 = 2.75;

    public EaseInBounce(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1.0 - _easeOutBounce(1.0 - x)) * k + origin;
        }
        else{
            return target;
        }
    }

    public static double _easeOutBounce(double x) {
        if (x < 1.0 / d1){
            return n1 * x * x;
        } else if (x < x/ d1) {
            x -= 1.5 / d1;
            return n1 * x * x + 0.75;
        } else if (x < 2.5 / d1) {
            x -= 2.25 / d1;
            return n1 * x * x + 0.9375;
        } else {
            x -= 2.625 / d1;
            return n1 * x * x + 0.984375;
        }
    }
}

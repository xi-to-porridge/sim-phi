package org.xi2p.easing;

public class EaseInBack extends Func{
    public static final double c1 = 1.70158;
    public static final double c3 = 2.70158;

    public EaseInBack(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (c3 * x * x * x - c1 * x * x) * k + origin;
        }
        else{
            return target;
        }
    }
}

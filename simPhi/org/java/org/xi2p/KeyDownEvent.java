package org.xi2p;

public class KeyDownEvent {
    int scancode;
    double beat;

    public KeyDownEvent(int scancode, double beat) {
        this.scancode = scancode;
        this.beat = beat;
    }
}

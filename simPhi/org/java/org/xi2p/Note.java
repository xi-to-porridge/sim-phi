package org.xi2p;

public class Note{
    public Judgeline judgeline;
    public int type;
    public double x;
    public double at;
    public double end;
    public Boolean above;
    public double alpha;
    public Boolean fake;
    public double speed;
    public Boolean highlight = false;
    public double yInSurface = -1;
    public double xInSurface = -1;
    public long id = -1;
    double total_y;

    Evaluation evaluation = null;
    boolean miss = false;
    public long lastEvalTime = -1;

    public Note(Judgeline judgeline, int type, double x, double at, double end, Boolean above, double alpha, Boolean fake, double speed) {
        this.type = type;
        this.judgeline = judgeline;
        this.x = x;
        this.at = at;
        this.end = end;
        this.above = above;
        this.alpha = alpha;
        this.fake = fake;
        this.speed = speed;
        if (fake) {
            highlight = true;
        }
        total_y = judgeline.noteYObject.getValue(at);

    }

    public void upgrade(double beat){
        // 更新此note的中心点坐标

        double _y = (total_y - judgeline.noteYObject.getValue(beat)) * (above ? 1 : -1) * speed;
        double r = Math.pow(
                Math.pow(x, 2) + Math.pow(_y, 2), 0.5
        );

        double _angle;
        if (x > 0){
            _angle = judgeline.angle + Math.toDegrees(
                    Math.atan(_y / x)
            );
        } else if (x < 0) {
            _angle = judgeline.angle + Math.toDegrees(
                    Math.atan(_y / x)
            ) + 180;
        } else{
            _angle = judgeline.angle + (
                    _y >= 0 ? 90 : -90
                    );
        }

        xInSurface = r * Math.cos(Math.toRadians(_angle)) + judgeline.x;
        yInSurface = -r * Math.sin(Math.toRadians(_angle)) + judgeline.y;

    }

    public String blit(double beat){
        // type x1 y1 x2 y2 x3 y3 x4 y4 r g b alpha
        double r=0.0;
        double g=0.0;
        double b=0.0;

        switch (type) {
            case 1 -> {
                r = 10 + (highlight ? 64 : 0);
                g = 180 + (highlight ? 64 : 0);
                b = 240 + (highlight ? 15 : 0);
            }
            case 2 -> {
                r = 211 + (highlight ? 44 : 0);
                g = 211 + (highlight ? 44 : 0);
                b = 105 + (highlight ? 64 : 0);
            }
            case 3 -> {
                r = 191 + (highlight ? 64 : 0);
                g = 40 + (highlight ? 64 : 0);
                b = 110 + (highlight ? 64 : 0);
            }
        }
        double x1, y1, x2, y2, x3, y3, x4, y4;
        x1 = xInSurface + core.NOTE_R * Math.cos(Math.toRadians(judgeline.angle + core.NOTE_THETA));
        y1 = yInSurface - core.NOTE_R * Math.sin(Math.toRadians(judgeline.angle + core.NOTE_THETA));

        x2 = xInSurface + core.NOTE_R * Math.cos(Math.toRadians(judgeline.angle + 180 - core.NOTE_THETA));
        y2 = yInSurface - core.NOTE_R * Math.sin(Math.toRadians(judgeline.angle + 180 - core.NOTE_THETA));

        x3 = xInSurface + core.NOTE_R * Math.cos(Math.toRadians(judgeline.angle - 180 + core.NOTE_THETA));
        y3 = yInSurface - core.NOTE_R * Math.sin(Math.toRadians(judgeline.angle - 180 + core.NOTE_THETA));

        x4 = xInSurface + core.NOTE_R * Math.cos(Math.toRadians(judgeline.angle - core.NOTE_THETA));
        y4 = yInSurface - core.NOTE_R * Math.sin(Math.toRadians(judgeline.angle - core.NOTE_THETA));

        double alpha = 255.0 * (1 - (beat - at) / core.GOOD);
        if (alpha > 255.0) alpha = 255.0;
        if (alpha < 0.0) alpha = 0.0;
        return "%d %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f|".formatted(
//                type, x1, y1, x2, y2, x3, y3, x4, y4, r, g, b, 255.0
                type, x1, y1, x2, y2, x3, y3, x4, y4, r, g, b, alpha
        );
    }

}

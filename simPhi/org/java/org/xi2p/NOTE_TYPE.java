package org.xi2p;

public enum NOTE_TYPE {
    TAP, DRAG, FLICK, HOLD
}

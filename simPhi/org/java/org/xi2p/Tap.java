package org.xi2p;

public class Tap extends Note{
    public Tap(Judgeline judgeline, int type, double x, double at, double end, Boolean above, double alpha, Boolean fake, double speed) {
        super(judgeline, type, x, at, end, above, alpha, fake, speed);
    }
}

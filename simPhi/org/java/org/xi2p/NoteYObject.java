package org.xi2p;

import org.xi2p.easing.Func;

public class NoteYObject extends AlterObject{
    public NoteYObject() {
    }

    @Override
    public double getValue(double beat) {
        for (Func event: events){
            if (event.startBeat > beat){
                break;
            }

            if (event.endBeat < beat){
                value = event.target;
            } else{
                value = event.calculate(beat);
            }

        }

//        while (!events.isEmpty() && events.get(0).endBeat < beat){
//            events.remove(0);
//        }
//         events.removeIf(func -> func.endBeat < beat);
//         removeIf 效率较低，因为会遍历整个列表

        return value;
    }
}

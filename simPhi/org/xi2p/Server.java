package org.xi2p;


import java.io.*;
import java.net.*;

public class Server{
    public static void main(String[] args)throws Exception {
        ServerSocket serverSocket = new ServerSocket(1222);
        System.out.println("Server started. ");
        System.out.println("Server occupies Port 1222");
        System.out.println("Server dir: " + System.getProperty("user.dir"));
        while(true){
            Socket socket = serverSocket.accept();
            System.out.println("Data stream detected. ");
            InputStream inputStream = socket.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String s = bufferedReader.readLine();
            OutputStream stream = socket.getOutputStream();
            core.printWriter = new PrintWriter(stream);
            core.badPainter = new BadPainter(core.printWriter);
            while(s!= null){
                // 响应传来的指令
                // s 不含最后的 \n
//                System.out.println(s);
                try {
                    core.respond(s);
//                    System.out.println();
                } catch (Exception e){
                    e.printStackTrace();
//                    System.out.println("500 BAD COMMAND BODY");
                }

                s = bufferedReader.readLine();
            }
            socket.close();
            System.out.println("Connect Shutdown");
            core.commandNum = 0;
        }
    }
}
package org.xi2p;

public class Evaluation {
    public static String GOOD = "GOOD";
    public static String PERFECT = "PERFECT";
    public static String MISS = "MISS";
    public static String BAD = "BAD";

    Note note;
    String evalString;

    public Evaluation(Note note, String evaluation) {
        this.note = note;
        this.evalString = evaluation;
    }
}

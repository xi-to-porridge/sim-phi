package org.xi2p;

import org.xi2p.easing.Code2FuncMap;
import org.xi2p.easing.Func;

import java.util.ArrayList;

public class AlterObject {
    double value = 0;
    ArrayList<Func> events = new ArrayList<>();

    public void addEvent(int key, double target, double origin, double endBeat, double startBeat){
        Func event = Code2FuncMap.get(key, target, origin, endBeat, startBeat);
        if (event != null){
            events.add(event);
        }
    }

    public double getValue(double beat){
        for (Func event: events){
//            if (event == null) {
//                System.out.println("WARNING: event is null in AlterObject.getValue : ");
//                System.out.print("[");
//                for (Func _event: events) {
//                    System.out.print(_event);
//                    System.out.print(", ");
//                }
//                System.out.println("]");
//            }
            if (event.startBeat > beat){
                break;
            }

            if (event.endBeat < beat){
                value = event.target;
            } else{
                value = event.calculate(beat);
            }

        }

        while (!events.isEmpty() && events.get(0).endBeat < beat){
            events.remove(0);
        }
        // events.removeIf(func -> func.endBeat < beat);
        // removeIf 效率较低，因为会遍历整个列表

        return value;
    }
}

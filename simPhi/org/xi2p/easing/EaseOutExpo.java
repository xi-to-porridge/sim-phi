package org.xi2p.easing;

public class EaseOutExpo extends Func{
    public EaseOutExpo(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }
    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x == 1) ? 1 : (1 - Math.pow(2, -10 * x))) * k + origin;
        }
        else{
            return target;
        }
    }
}

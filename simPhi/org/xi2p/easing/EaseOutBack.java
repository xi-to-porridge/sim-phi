package org.xi2p.easing;

public class EaseOutBack extends Func{
    private static final double c1 = 1.70158;
    private static final double c3 = 2.70158;
    public EaseOutBack(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1 + c3 * Math.pow(x - 1, 3) + c1 * Math.pow(x -1, 2)) * k + origin;
        }
        else{
            return target;
        }
    }
}

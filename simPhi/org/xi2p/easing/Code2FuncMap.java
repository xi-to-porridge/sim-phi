package org.xi2p.easing;

public class Code2FuncMap{
    public Code2FuncMap() {
    }

    public static Func get(int key, double target, double origin, double endBeat, double startBeat){
        if (key > 28) System.out.printf("WARNING: Invalid easing function key: %d%n", key);
        return switch (key) {
            case 1 -> new Liner(target, origin, endBeat, startBeat);
            case 2 -> new EaseOutSine(target, origin, endBeat, startBeat);
            case 3 -> new EaseInSine(target, origin, endBeat, startBeat);
            case 4 -> new EaseOutQuad(target, origin, endBeat, startBeat);
            case 5 -> new EaseInQuad(target, origin, endBeat, startBeat);
            case 6 -> new EaseInOutSine(target, origin, endBeat, startBeat);
            case 7 -> new EaseInOutQuad(target, origin, endBeat, startBeat);
            case 8 -> new EaseOutCubic(target, origin, endBeat, startBeat);
            case 9 -> new EaseInCubic(target, origin, endBeat, startBeat);
            case 10 -> new EaseOutQuart(target, origin, endBeat, startBeat);
            case 11 -> new EaseInQuart(target, origin, endBeat, startBeat);
            case 12 -> new EaseInOutCubic(target, origin, endBeat, startBeat);
            case 13 -> new EaseInOutQuart(target, origin, endBeat, startBeat);
            case 14 -> new EaseOutQuint(target, origin, endBeat, startBeat);
            case 15 -> new EaseInQuint(target, origin, endBeat, startBeat);
            case 16 -> new EaseOutExpo(target, origin, endBeat, startBeat);
            case 17 -> new EaseInExpo(target, origin, endBeat, startBeat);
            case 18 -> new EaseOutCirc(target, origin, endBeat, startBeat);
            case 19 -> new EaseInCirc(target, origin, endBeat, startBeat);
            case 20 -> new EaseOutBack(target, origin, endBeat, startBeat);
            case 21 -> new EaseInBack(target, origin, endBeat, startBeat);
            case 22 -> new EaseInOutCirc(target, origin, endBeat, startBeat);
            case 23 -> new EaseInOutBack(target, origin, endBeat, startBeat);
            case 24 -> new EaseOutElastic(target, origin, endBeat, startBeat);
            case 25 -> new EaseInElastic(target, origin, endBeat, startBeat);
            case 26 -> new EaseOutBounce(target, origin, endBeat, startBeat);
            case 27 -> new EaseInBounce(target, origin, endBeat, startBeat);
            case 28 -> new EaseInOutBounce(target, origin, endBeat, startBeat);
            default -> null;
        };
    }
}

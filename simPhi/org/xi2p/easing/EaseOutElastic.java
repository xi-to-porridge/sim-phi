package org.xi2p.easing;

public class EaseOutElastic extends Func{
    private static final double c4 = 2 * Math.PI / 3;
    public EaseOutElastic(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x == 0) ? 0 : ((x == 1) ? 1
                    : (Math.pow(2, -10*x) * Math.sin((x * 10 - 0.75) * c4) + 1))) * k + origin;
        }
        else{
            return target;
        }
    }
}

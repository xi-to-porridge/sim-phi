package org.xi2p.easing;

public class EaseInExpo extends Func {
    public EaseInExpo(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x == 0) ? 0 : Math.pow(2, 10 * x - 10)) * k + origin;
        }
        else{
            return target;
        }
    }
}

package org.xi2p.easing;

public class EaseOutSine extends Func{
    public EaseOutSine(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (Math.sin((x * Math.PI) / 2)) * k + origin;
        }
        else{
            return target;
        }
    }
}

package org.xi2p.easing;

public class EaseOutQuad extends Func{
    public EaseOutQuad(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return (1 - (1 - x) * (1 - x)) * k + origin;
        }
        else{
            return target;
        }
    }
}

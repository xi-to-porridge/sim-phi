package org.xi2p.easing;

public abstract class Func {
    public double target;
    public double origin;
    public double endBeat;
    public double startBeat;
    double k;

    public Func(double target, double origin, double endBeat, double startBeat) {
        this.target = target;
        this.origin = origin;
        this.endBeat = endBeat;
        this.startBeat = startBeat;
        this.k = target - origin;
    }

    public abstract double calculate(double beat);
}

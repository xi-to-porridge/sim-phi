package org.xi2p.easing;

public class EaseInOutCubic extends Func{
    public EaseInOutCubic(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }
    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x < 0.5) ? (4 * x * x * x) : (1 - Math.pow(-2 * x + 2, 3) / 2)) * k + origin;
        }
        else{
            return target;
        }
    }
}

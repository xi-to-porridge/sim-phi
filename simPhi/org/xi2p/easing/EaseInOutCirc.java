package org.xi2p.easing;

public class EaseInOutCirc extends Func{
    public EaseInOutCirc(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return ((x < 0.5) ? ((1 - Math.sqrt(1 - Math.pow(2 * x, 2))) / 2)
                    : ((Math.sqrt(1 - Math.pow(-2 * x + 2, 2)) + 1) / 2)) * k + origin;
        }
        else{
            return target;
        }
    }
}
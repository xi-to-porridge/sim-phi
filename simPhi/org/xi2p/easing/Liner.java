package org.xi2p.easing;

public class Liner extends Func{

    public Liner(double target, double origin, double endBeat, double startBeat) {
        super(target, origin, endBeat, startBeat);
    }

    @Override
    public double calculate(double beat) {
        if (beat < endBeat){
            double x = (beat - startBeat) / (endBeat - startBeat);
            return x * k + origin;
        }
        else{
            return target;
        }
    }

}

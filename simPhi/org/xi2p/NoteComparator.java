package org.xi2p;

import java.util.Comparator;

public class NoteComparator<E> implements Comparator<E> {
    @Override
    public int compare(Object o1, Object o2) {
        double deltaA = ((Note) o1).at - ((Note) o2).at;

        double deltaT = getT((Note) o1) - getT((Note) o2);
        if (deltaA > 0){
            return 2;
        } else if (deltaA < 0) {
            return -2;
        } else {
            if (deltaT > 0){
                return -1;
            } else if (deltaT < 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    private int getT(Note note){
        return switch (note.type){
            case 1 -> 1;
            case 2, 3 -> 0;
            case 4 -> 4;
            default -> -1;
        };
    }
}

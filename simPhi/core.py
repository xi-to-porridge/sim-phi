import json

from PIL import Image, ImageFilter, ImageEnhance
import pygame
import typing as T
import os
import socket
import subprocess
import time
import zipfile

import easing

pygame.init()
pygame.mixer.init()

BREAK = 0
CONTINUE = 1

WIDTH = 854
HEIGHT = 481

if not os.path.exists("./cache"):
    os.makedirs("./cache")


class Eval:
    GOOD = '0'
    PERFECT = '1'
    BAD = "2"

    def __init__(self):
        self.goods = [
            pygame.image.load(f"./resources/texture/img-{i}_good.png") for i in range(1, 31)
        ]
        self.perfects = [
            pygame.image.load(f"./resources/texture/img-{i}_perfect.png") for i in range(1, 31)
        ]

        for index in range(len(self.goods)):
            self.goods[index] = pygame.transform.smoothscale(
                self.goods[index], (self.goods[index].get_width() / 1.5, self.goods[index].get_height() / 1.5)
            )

        for index in range(len(self.perfects)):
            self.perfects[index] = pygame.transform.smoothscale(
                self.perfects[index], (self.perfects[index].get_width() / 1.5, self.perfects[index].get_height() / 1.5)
            )

    def __getitem__(self, item: T.Tuple[str, int]):
        """
        获取反馈图像
        :param item: (TYPE, INDEX)
        :return: EvalImg Surface Object
        """

        return (self.goods if item[0] == Eval.GOOD else self.perfects)[item[1]]


class Texture:
    Tap = 0
    TapHL = 1
    Drag = 2
    DragHL = 3
    Flick = 4
    FlickHL = 5
    Hold = 6
    Line = 7
    EvalImg = 8
    Phi = 9
    A = 10
    B = 11
    C = 12
    S = 13
    V = 14
    VFC = 15
    GRAY_BG = 16

    def __init__(self):
        self.id2texture = {
            Texture.Tap: pygame.image.load("./resources/texture/Tap2.png"),
            Texture.TapHL: pygame.image.load("./resources/texture/Tap2HL.png"),
            Texture.Drag: pygame.image.load("./resources/texture/Drag2.png"),
            Texture.DragHL: pygame.image.load("./resources/texture/DragHL.png"),
            Texture.Flick: pygame.image.load("./resources/texture/Flick2.png"),
            Texture.FlickHL: pygame.image.load("./resources/texture/Flick2HL.png"),
            Texture.Hold: pygame.image.load("./resources/texture/Hold2.png"),
            Texture.Line: pygame.image.load("./resources/texture/line.png"),
            Texture.EvalImg: Eval(),
            Texture.Phi: pygame.image.load("./resources/texture/phi.png"),
            Texture.A: pygame.image.load("./resources/texture/a.png"),
            Texture.B: pygame.image.load("./resources/texture/b.png"),
            Texture.C: pygame.image.load("./resources/texture/c.png"),
            Texture.S: pygame.image.load("./resources/texture/s.png"),
            Texture.V: pygame.image.load("./resources/texture/v.png"),
            Texture.VFC: pygame.image.load("./resources/texture/vfc.png")
        }

        for key in self.id2texture:
            if key in [Texture.EvalImg, Texture.Line, Texture.Hold]:
                continue

            self.id2texture[key] = pygame.transform.smoothscale(
                self.id2texture[key], (self.id2texture[key].get_width() / 8, self.id2texture[key].get_height() / 8)
            )

        gray_bg = pygame.image.load("./resources/texture/gray.png")

        gray_bg = pygame.transform.scale(gray_bg,
                                         (int(WIDTH), int(HEIGHT))
                                         )

        gray_bg.set_alpha(200)

        self.id2texture[Texture.GRAY_BG] = gray_bg

    def __getitem__(self, item: int):
        """
        获取反馈图像
        :param item: (TYPE, INDEX)
        :return: EvalImg Surface Object
        """

        return self.id2texture[item]


Texture = Texture()

#  = = = = = GAME SETTINGS = = = = =
DEBUG = True
NO_CACHE = False
ENABLE_SOUND = True
MODE = "autoplay"
RESOURCES_DIR = "../downloaded_beatmap"
PAUSE = False
OFFSET = 0
KEYBOARD_OFFSET = 0
# AUTO_BOOT_SERVER = False
AUTO_BOOT_SERVER = True
#  = = = = = GAME SETTINGS = = = = =

#  = = = = = PYGAME CONST = = = = =
TAP_SOUND = pygame.mixer.Sound("./resources/audio/tap.wav")
DRAG_SOUND = pygame.mixer.Sound("./resources/audio/drag.wav")
FLICK_SOUND = pygame.mixer.Sound("./resources/audio/flick.wav")

font50 = pygame.font.Font("./resources/cmdysj.ttf", 50)
font40 = pygame.font.Font("./resources/cmdysj.ttf", 40)
font30 = pygame.font.Font("./resources/cmdysj.ttf", 30)
font25 = pygame.font.Font("./resources/cmdysj.ttf", 25)
font20 = pygame.font.Font("./resources/cmdysj.ttf", 20)

screen = pygame.display.set_mode((WIDTH, HEIGHT))
surface = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
# surface.set_alpha(200)
pygame.display.set_caption("SimPhi v1.2.0 - Code by xi2p")
pause_surface = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
welcome_text1 = font50.render("欢迎来到SimPhi!", True, (255, 255, 255))
welcome_text2 = font30.render("初始化中...", True, (255, 255, 255))
unpause_text_1 = font30.render("Press Esc to unpause...", True, (200, 200, 200))
unpause_text_2 = font30.render("[Esc] Continue    [Q] Quit", True, (200, 200, 200))
name_text = font25.render("| ", True, (255, 255, 255))  # PLAYING时左下角
title_text = font40.render("", True, (255, 255, 255))  # 其他时刻显示的标题
level_text = font25.render("LEVEL. ?", True, (255, 255, 255))
copyright_text = font20.render("SimPhi Project - Code by xi2p", True, (200, 200, 200))
composer_text = font25.render("", True, (255, 255, 255))
charter_text = font25.render("", True, (255, 255, 255))
background: T.Union[pygame.Surface, None] = None

# - - - MODIFIED IN END_1 - - -
background_clear: T.Union[pygame.Surface, None] = None
phi_image = pygame.image.load("./resources/texture/phi.png")
score_text = font40.render("1000000", True, (255, 255, 255))
max_combo_text = font20.render(f"ALL PERFECT", True, (240, 10, 100))
perfect_text = font20.render("Perfect", True, (255, 255, 255))
good_text = font20.render("Good", True, (255, 255, 255))
bad_text = font20.render("Bad", True, (255, 255, 255))
miss_text = font20.render("Miss", True, (255, 255, 255))
perfect_num_text = font20.render(f"0", True, (255, 255, 255))
good_num_text = font20.render("0", True, (255, 255, 255))
bad_num_text = font20.render("0", True, (255, 255, 255))
miss_num_text = font20.render("0", True, (255, 255, 255))
back_text = font25.render("Press [Enter] to exit...", True, (255, 255, 255))
# - - - MODIFIED IN END_1 - - -

# - - - MODIFIED IN PLAYING_USER - - -
combo_text = font30.render("COMBO", True, (255, 255, 255))
combo_num_text = font40.render(str(0), True, (255, 255, 255))
score_text = font30.render(str(0).rjust(7, '0'), True, (255, 255, 255))
fps_text = font25.render(str(0).rjust(3, "0"), True, (255, 255, 255))
offset_text = font20.render(f"OFFSET={OFFSET}", True, (255, 255, 255))
# - - - MODIFIED IN PLAYING_USER - - -

clock = pygame.time.Clock()

#  = = = = = PYGAME CONST = = = = =

#  = = = = = RENDER CONST = = = = =
NOTE_X_SCALE = 220 / 300 * WIDTH / 1000
LINE_X_SCALE = 0.8 * WIDTH / 1000
LINE_Y_SCALE = -38 / 50 * HEIGHT / 700
SPEED_SCALE = 25
evalPainter: T.Any = None
#  = = = = = RENDER CONST = = = = =

#  = = = = = BEATMAP DATA = = = = =
NOTE_NUM = 0
DURATION = 0
NAME = ''
COMPOSER = ''
CHARTER = ''
LEVEL = ''
IMAGE: os.PathLike
SONG: os.PathLike
BeatObject: object  # 提供秒转拍服务
#  = = = = = BEATMAP DATA = = = = =

#  = = = = = SCENES CONST = = = = =
PREPARE_DURATION = 3
END_DURATION_1 = 3
END_DURATION_2 = 0.8

SELECT = -1
PREPARE = 0
PLAYING = 1
END_1 = 2
END_2 = 3

scene = SELECT

t2x: T.Union[easing.Func, None] = None
#  = = = = = SCENES CONST = = = = =

#  = = = = = GAME DATA = = = = =
beat = 0
start = -1

perfect_num = 0
good_num = 0
miss_num = 0
bad_num = 0
score = 0
combo = 0
max_combo = 0

#  = = = = = GAME DATA = = = = =

#  = = = = = BEATMAP SELECT = = = = =
if not os.path.exists("./beatmaps"):
    os.mkdir("./beatmaps")
_beatmaps = os.listdir("./beatmaps")
beatmaps = []
beatmap_index = 0

for _beatmap in _beatmaps:
    _beatmap = "./beatmaps/" + _beatmap
    if os.path.isdir(_beatmap):
        beatmaps.append(_beatmap)

if beatmaps:
    selected_beatmap = beatmaps[beatmap_index % len(beatmaps)]
else:
    selected_beatmap = ''

# screen = pygame.display.set_mode((WIDTH, HEIGHT))
# surface = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
select_title: T.Union[pygame.Surface, None] = None
select_wait_text = pygame.font.Font("resources/cmdysj.ttf", 50).render("加载中...", True, (255, 255, 255))
select_import_text1 = pygame.font.Font("resources/cmdysj.ttf", 20).render("按下[i]以导入铺面      "
                                                                          "按下[d]以删除铺面       ",
                                                                          True,
                                                                          (255, 255, 255),)

select_import_text2 = pygame.font.Font("resources/cmdysj.ttf", 20).render("按下[Enter]以开始    "
                                                                          "按下[↑↓]以切换模式",
                                                                          True,
                                                                          (255, 255, 255),)

select_import_text3 = pygame.font.Font("resources/cmdysj.ttf", 20).render("按下[←→]以切换铺面    "
                                                                          "                                         ",
                                                                          True,
                                                                          (255, 255, 255),)

select_importing_text = pygame.font.Font("resources/cmdysj.ttf", 50).render("导入中...",
                                                                            True,
                                                                            (255, 255, 255))

if selected_beatmap:
    for file_name in os.listdir(selected_beatmap):
        if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
            # background = pygame.image.load(f"cache/{file_name}")

            pil_blurred = Image.open(f"{selected_beatmap}/{file_name}").convert("RGB").filter(
                ImageFilter.GaussianBlur(radius=15))
            bright_enhancer = ImageEnhance.Brightness(pil_blurred)
            pil_blurred = bright_enhancer.enhance(0.5)

            background = pygame.transform.smoothscale(
                pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                (WIDTH, HEIGHT)
            )
            break
            # # img = Image.f
            # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

    for file_name in os.listdir(selected_beatmap):
        if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
            fp = open(f"{selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
            chart_json = json.load(fp)
            fp.close()
            # 加载基本信息
            NAME = chart_json["META"]["name"]
            COMPOSER = chart_json["META"]["composer"]
            CHARTER = chart_json["META"]["charter"]
            LEVEL = chart_json["META"]["level"]

            select_title = font50.render(NAME, True, (255, 255, 255), (128, 128, 128, 128))
            composer_text = font30.render("Composer: " + COMPOSER,
                                          True, (255, 255, 255))
            charter_text = font30.render("Charter: " + CHARTER,
                                         True, (255, 255, 255))
            level_text = font30.render(LEVEL,
                                       True, (255, 255, 255))
            break
#  = = = = = BEATMAP SELECT = = = = =

#  = = = = = SERVER CONFIG = = = = =
screen.blit(welcome_text1, (WIDTH/2-welcome_text1.get_width()/2, HEIGHT/2-welcome_text1.get_height()/2 - 30))
screen.blit(welcome_text2, (WIDTH/2-welcome_text2.get_width()/2, HEIGHT/2-welcome_text2.get_height()/2 + 10))
screen.blit(copyright_text, (WIDTH - copyright_text.get_width() - 5,
                                        HEIGHT - copyright_text.get_height() - 5))
pygame.display.flip()
host = 'localhost'
port = 1222
client: T.Union[None, socket.socket] = None


def send_to_server(msg: str):
    # print("-> ",msg)
    client.send((msg + "\n").encode())


def read_server():
    string = ""
    while "$" not in string:
        string += client.recv(1).decode()
    # print("<-", string)
    return string


def start_server():
    global client
    if AUTO_BOOT_SERVER:
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)  # 在客户端开启心跳维护
            client.connect((host, port))
            send_to_server("shutdown")
            time.sleep(3)
        except:
            pass

        # 寻找java jdk路径

        subprocess.Popen("\"C:/Program Files/Common Files/Oracle/Java/javapath/java\" org.xi2p.Server", shell=True)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)  # 在客户端开启心跳维护
        client.connect((host, port))
    else:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)  # 在客户端开启心跳维护
        client.connect((host, port))


start_server()
#  = = = = = SERVER CONFIG = = = = =

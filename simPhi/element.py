import time
import typing as T
import alterobj
import core


class JudgeLine:
    # 欺骗指数。用于暂时解决note出现在屏幕外就中断整个判定线的绘制的问题
    CHEAT_INDICATOR = 1

    # 判定线
    def __init__(self):
        """
        初始化判定线
        """
        # self.x = 0
        # self.y = 0
        # self.angle = 0
        # self.alpha = 0
        # self.id = -1
        #
        # self.x_object: T.Union[alterobj.LineXObject, None] = None
        # self.y_object: T.Union[alterobj.LineYObject, None] = None
        # self.alpha_object: T.Union[alterobj.AlphaObject, None] = None
        # self.angle_object: T.Union[alterobj.AngleObject, None] = None
        # # self.speed_object: T.Union[alterobj.LineSpeedObject, None] = None
        # self.note_y_object: T.Union[alterobj.NoteYObject, None] = None
        # # if self.speed_object:
        # #     self.notes_y_object = self.speed_object.get_y_object()
        # # else:
        # #     self.notes_y_object = alterobj.FakeNoteYObject(speed)
        # self.notes = []
        # # clean-up
        #
        # self.holds = []
        # self.not_holds = []
        # self.above1 = []
        # self.above2 = []


class Note:
    TAP = 1
    DRAG = 2
    FLICK = 3
    HOLD = 4

    # 所有音符的超类
    def __init__(self, judge_line, x=0, at=0, above=True, alpha=0, end=-1, fake=False):
        """
        初始化音符
        :param x: 初始x坐标
        :param at: 打击时间
        :param above: 是否从判定线上方下落
        :param alpha: 透明度
        :param end: 如果Note是Hold，则本属性为结束打击时间
        :param fake: 真假Note
        """
        self.judge_line = judge_line
        self.x = x
        self.at = at
        self.end = end
        self.angle = 0 if above else 180
        self.above = above
        self.alpha = alpha
        self.fake = fake
        self.id = -1
        self.highlight = False  # 是否双押或表演(高光)
        self.y_in_surface = -1
        self.x_in_surface = -1
        self.speed = 1.0

    def __str__(self):
        return f"{self.id} {self.x} {1 if self.above else 0} {self.alpha} {self.at} {self.end} {1 if self.fake else 0} {self.speed}"


class Tap(Note):
    # 所有音符的超类
    def __init__(self, judge_line, x=0, at=0, above=True, alpha=0, end=-1, fake=False):
        super().__init__(judge_line, x, at, above, alpha, end, fake)
        self.id = Note.TAP


class Drag(Note):
    # 所有音符的超类
    def __init__(self, judge_line, x=0, at=0, above=True, alpha=0, end=-1, fake=False):
        super().__init__(judge_line, x, at, above, alpha, end, fake)
        self.id = Note.DRAG


class Flick(Note):
    # 所有音符的超类
    def __init__(self, judge_line, x=0, at=0, above=True, alpha=0, end=-1, fake=False):
        super().__init__(judge_line, x, at, above, alpha, end, fake)
        self.id = Note.FLICK


class Hold(Note):
    # 所有音符的超类
    def __init__(self, judge_line, x=0, at=0, above=True, alpha=0, end=-1, fake=False):
        super().__init__(judge_line, x, at, above, alpha, end, fake)
        self.id = Note.HOLD
        self.duration = self.end - self.at
        self.last_eval_time = -1
        self.length = 0


class EvalPainter:
    # 持续时长
    DURATION = 0.5

    def __init__(self):
        # [(<Tap Object>, 1.254418), ...]
        self.notes_time_eval: T.List[T.Tuple[float, float, float, str]] = []

    def add_note(self, x: float, y: float, eval_: str):
        """
        添加note
        :param eval_: core.Eval.GOOD / core.Eval.PERFECT
        :return:
        """
        self.notes_time_eval.append((x, y, time.time(), eval_))

    def blit(self, surface):
        while self.notes_time_eval and (time.time() - self.notes_time_eval[0][2]) >= EvalPainter.DURATION:
            self.notes_time_eval.pop(0)

        for x, y, time_, eval_ in self.notes_time_eval:
            texture = core.Texture[core.Texture.EvalImg][eval_,
                                                         min(int(29 * (time.time() - time_) / EvalPainter.DURATION),
                                                             29)]
            surface.blit(
                texture, (x - texture.get_width() / 2, y - texture.get_height() / 2)
            )


if __name__ == '__main__':
    pass

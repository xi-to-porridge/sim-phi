import pygame
import os
import core
import sys
import zipfile
from PIL import Image, ImageFilter
import typing as T

RESOURCES_DIR = "../downloaded_beatmap"

if RESOURCES_DIR.endswith("/") or RESOURCES_DIR.endswith("\\"):
    RESOURCES_DIR = RESOURCES_DIR[:-1]

_zips = os.listdir(RESOURCES_DIR)
zips = []
zip_index = 0

for _zip in _zips:
    _zip = RESOURCES_DIR + os.sep + _zip
    if zipfile.is_zipfile(_zip):
        zips.append(_zip)

if zips:
    selected_zip = zips[zip_index % len(zips)]
else:
    selected_zip = ''

screen = pygame.display.set_mode((core.WIDTH, core.HEIGHT))
surface = pygame.Surface((core.WIDTH, core.HEIGHT), pygame.SRCALPHA)
background: T.Union[pygame.Surface, None] = None
title: T.Union[pygame.Surface, None] = None
font = pygame.font.Font("resources/cmdysj.ttf", 30)
wait_text = pygame.font.Font("resources/cmdysj.ttf", 50).render("加载中...", True, (255, 255, 255))

archive = zipfile.ZipFile(selected_zip)
for file_name in archive.namelist():
    if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
        archive.extract(file_name, "cache")
        # background = pygame.image.load(f"cache/{file_name}")

        pil_blurred = Image.open(f"cache/{file_name}").filter(ImageFilter.GaussianBlur(radius=15))
        # bright_enhancer = ImageEnhance.Brightness(pil_blurred)
        # img = bright_enhancer.enhance(0.5)
        # pil_blurred.convert("RGB").save("./cache/shot.jpg")

        background = pygame.transform.smoothscale(
            pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
            (core.WIDTH, core.HEIGHT)
        )
        title = font.render(os.path.basename(selected_zip), True, (255, 255, 255), (50, 50, 50, 100))
        break
        # # img = Image.f
        # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            core.send_to_server("shutdown")
            sys.exit(0)
        if event.type == pygame.KEYDOWN:
            print(event.scancode)
            if event.scancode == 80:
                surface.fill((128, 128, 128, 100))
                surface.blit(wait_text, (core.WIDTH/2 - wait_text.get_width()/2,
                                         core.HEIGHT/2 - wait_text.get_height()/2))
                screen.blit(surface, (0, 0))
                pygame.display.flip()


                zip_index += 1
                selected_zip = zips[zip_index % len(zips)]

                archive = zipfile.ZipFile(selected_zip)

                for file_name in archive.namelist():
                    if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                        archive.extract(file_name, "cache")
                        # background = pygame.image.load(f"cache/{file_name}")

                        pil_blurred = Image.open(f"cache/{file_name}").filter(ImageFilter.GaussianBlur(radius=15))
                        # bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                        # img = bright_enhancer.enhance(0.5)
                        # pil_blurred.convert("RGB").save("./cache/shot.jpg")

                        background = pygame.transform.smoothscale(
                            pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                            (core.WIDTH, core.HEIGHT)
                        )
                        title = font.render(os.path.basename(selected_zip), True, (255, 255, 255), (50, 50, 50, 100))
                        break
            elif event.scancode == 79:
                surface.fill((128, 128, 128, 100))
                surface.blit(wait_text, (core.WIDTH/2 - wait_text.get_width()/2,
                                         core.HEIGHT/2 - wait_text.get_height()/2))
                screen.blit(surface, (0, 0))
                pygame.display.flip()
                zip_index -= 1
                selected_zip = zips[zip_index % len(zips)]

                archive = zipfile.ZipFile(selected_zip)
                for file_name in archive.namelist():
                    if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                        archive.extract(file_name, "cache")
                        # background = pygame.image.load(f"cache/{file_name}")

                        pil_blurred = Image.open(f"cache/{file_name}").filter(ImageFilter.GaussianBlur(radius=15)).convert("RGB")
                        # bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                        # img = bright_enhancer.enhance(0.5)
                        # pil_blurred.convert("RGB").save("./cache/shot.jpg")

                        background = pygame.transform.smoothscale(
                            pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                            (core.WIDTH, core.HEIGHT)
                        )
                        title = font.render(os.path.basename(selected_zip), True, (255, 255, 255), (50, 50, 50, 100))
                        break
    screen.fill((0, 0, 0))
    if background is not None:
        surface.blit(background, (0, 0))
        surface.blit(title, (20, core.HEIGHT - title.get_height() - 20))
        screen.blit(surface, (0, 0))
    pygame.display.flip()

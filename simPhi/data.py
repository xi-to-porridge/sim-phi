import csv
import gc
import json.decoder
import shutil
import core
import alterobj
import os
from json import load
import zipfile
import mc2rpe
import pec2rpe
import hashlib
import json2rpe


def list2beat(_list):
    return _list[0] + _list[1] / _list[2]


def get_rpe_meta(rpe_path):
    fp = open(rpe_path, 'r', encoding="UTF-8")
    chart_json = load(fp)
    fp.close()

    # 加载基本信息
    return {
        "name": chart_json["META"]["name"],
        "composer": chart_json["META"]["composer"],
        "charter": chart_json["META"]["charter"],
        "level": chart_json["META"]["level"],
        "image": chart_json["META"]["background"],
        "song": chart_json["META"]["song"]
    }


def load_rpe(rpe_path):
    # backslash = "\\"
    # core.send_to_server(f"init {os.path.realpath('resources/audio/tap.wav').replace(backslash, '/')} "
    #                     f"{os.path.realpath('resources/audio/drag.wav').replace(backslash, '/')} "
    #                     f"{os.path.realpath('resources/audio/flick.wav').replace(backslash, '/')}")

    fp = open(rpe_path, 'r', encoding="UTF-8")
    chart_json = load(fp)
    fp.close()

    note_num = 0

    # 加载基本信息
    core.DURATION = 999
    core.NAME = chart_json["META"]["name"]
    core.COMPOSER = chart_json["META"]["composer"]
    core.CHARTER = chart_json["META"]["charter"]
    core.LEVEL = chart_json["META"]["level"]
    core.IMAGE = chart_json["META"]["background"]
    core.SONG = chart_json["META"]["song"]
    core.OFFSET = chart_json["META"]["offset"]

    # 加载 秒拍转换
    core.BeatObject = alterobj.BeatObject(
        chart_json["BPMList"]
    )

    bpm = chart_json['BPMList'][0]['bpm']

    core.send_to_server(f"init {core.MODE} {bpm} {core.KEYBOARD_OFFSET / 1000 * bpm / 60}")

    x_scale = core.NOTE_X_SCALE

    type2id = {2: 4, 1: 1, 3: 3, 4: 2}
    print("reading and sending data to server ... ", end='')
    for judgeline_data in chart_json["judgeLineList"]:
        core.send_to_server("cj")

        alterobj.LineXObject(judgeline_data["eventLayers"][0]["moveXEvents"])
        alterobj.LineYObject(judgeline_data["eventLayers"][0]["moveYEvents"])
        alterobj.AngleObject(judgeline_data['eventLayers'][0]['rotateEvents'])
        alterobj.AlphaObject(judgeline_data['eventLayers'][0]['alphaEvents'])
        alterobj.NoteYObject(judgeline_data['eventLayers'][0]['speedEvents'])

        if judgeline_data.get("notes", False):
            for note_data in judgeline_data["notes"]:
                if not note_data["isFake"]:
                    note_num += 1
                # 2 -> Hold     1 -> Tap        3 -> Flick      4 -> Drag
                note_string = f"{type2id[note_data['type']]}" \
                              f" {note_data['positionX'] * x_scale}" \
                              f" {1 if (True if note_data['above'] == 1 else False) else 0}" \
                              f" {note_data['alpha']}" \
                              f" {list2beat(note_data['startTime'])}" \
                              f" {list2beat(note_data['endTime'])}" \
                              f" {1 if (True if note_data['isFake'] else False) else 0}" \
                              f" {note_data['speed']}"

                core.send_to_server("an " + note_string)

    core.NOTE_NUM = note_num

    gc.collect()
    print('done')


def is_rpe(rpe_path):
    try:
        fp = open(rpe_path, 'r', encoding="UTF-8")
        chart_json = load(fp)
        fp.close()

        # 检查基本信息
        _ = chart_json["META"]["name"]
        _ = chart_json["META"]["composer"]
        _ = chart_json["META"]["charter"]
        _ = chart_json["META"]["level"]
        _ = chart_json["META"]["background"]
        _ = chart_json["META"]["song"]
        _ = chart_json["META"]["offset"]

        # 加载 秒拍转换
        _ = alterobj.BeatObject(
            chart_json["BPMList"]
        )

        _ = chart_json['BPMList'][0]['bpm']

        # 检查一下文件头就差不多了吧
        return True
    except:
        return False


def is_pec(pec_path):
    try:
        pec_file = open(pec_path, "r", encoding="UTF-8")
        line = pec_file.readline().strip()
        pec_file.close()
        _ = int(line)
        return True
    except:
        return False


def is_pigeon(json_path):
    try:
        a = json_path

        file_json = open(a, "r", encoding="utf-8")
        file_json = json.load(file_json)

        # 写入bpm
        _ = {"bpm": file_json["judgeLineList"][0]["bpm"], "startTime": [0, 0, 1]}
        # 遍历
        count = 0
        for judgeline in file_json["judgeLineList"][:3]:
            # 判定线不透明度
            _ = judgeline["judgeLineDisappearEvents"]
            _ = judgeline["judgeLineMoveEvents"]
            _ = judgeline["judgeLineRotateEvents"]
            _ = judgeline["speedEvents"]
            _ = judgeline["notesAbove"]
            _ = judgeline["notesBelow"]
        return True
    except:
        return False


def is_mc(mc_path):
    try:
        with open(mc_path, "r") as f:
            mc = json.load(f)

        _ = {
            "charter": mc["meta"]["creator"],
            "composer": mc["meta"]["song"]["artist"],
            "id": f"m.{mc['meta']['id']}",
            "level": mc["meta"]["version"],
            "name": mc["meta"]["song"]["title"],
            "offset": -mc["note"][-1]["offset"],
        }

        _ = mc["meta"]["mode_ext"]["column"]

        # 检查一下文件头就好

        return True
    except:
        return False


# def load_dir(dir_path):
#     if not os.path.exists("./cache/temp"):
#         os.mkdir("./cache/temp")
#
#     # 先压缩，并获取md5
#     if os.path.exists("./cache/temp.zip"):
#         os.remove("./cache/temp.zip")
#
#     with zipfile.ZipFile("./cache/temp.zip", 'w', zipfile.ZIP_STORED) as temp_zip:
#         files = os.listdir(dir_path)
#         for file in files:
#             temp_zip.write(dir_path + ("" if dir_path[-1] in ['\\', '/'] else "/") + file, arcname=file)
#
#     load_zip("./cache/temp.zip")


def load_beatmap(md5):
    print(f"loading beatmap[md5={md5}]")

    chart = ''
    for file in os.listdir(f"./beatmaps/{md5}"):
        if file.split(".")[-1].lower() in ["json", "pec", "mc"]:
            chart = file

    load_rpe(f"./beatmaps/{md5}/{chart}")


# def load_beatmap(path):
#     if os.path.isdir(path):
#         load_dir(path)
#     else:
#         try:
#             load_zip(path)
#         except zipfile.BadZipFile:
#             mc2rpe.convert(path)
#             print("MC FORMAT DETECTED")
#             load_beatmap("./cache/m2pCache")
#
#
# def import_beatmap(path):
#     if os.path.isdir(path):
#         load_dir(path)
#     else:
#         try:
#             load_zip(path)
#         except zipfile.BadZipFile:
#             mc2rpe.convert(path)
#             print("MC FORMAT DETECTED")
#             import_beatmap("./cache/m2pCache")


def import_zip(zip_dir):
    print(f"importing file: {zip_dir}")
    if not zipfile.is_zipfile(zip_dir):
        print("Not a zip file!")
        raise zipfile.BadZipFile(
            "Not a zip file!"
        )
    print(f"checking zip md5 ...    ", end="")
    # 先验证md5
    file_object = open(zip_dir, 'rb')
    file_content = file_object.read()
    file_object.close()
    file_md5 = hashlib.md5(file_content)
    md5 = file_md5.hexdigest()
    # print(md5)

    if os.path.exists(f"./beatmaps/{md5}"):
        shutil.rmtree(f"./beatmaps/{md5}")

    if os.path.exists("./cache/temp"):
        shutil.rmtree("./cache/temp")

    os.makedirs("./cache/temp")

    print("extracting zip files ...    ", end='')
    with zipfile.ZipFile(zip_dir, mode="r") as temp_zip:
        temp_zip.extractall("./cache/temp")
    print("done")

    print("reading files ...    ", end='')

    # 读取 info, 获取铺面文件路径
    chart = ''
    song = ''
    picture = ''
    name = ''
    level = ''
    composer = ''
    illustrator = ''
    charter = ''

    if os.path.exists("./cache/temp/info.txt"):
        try:
            with open("./cache/temp/info.txt", "r", encoding="gbk") as f:
                # 第一行是 #
                f.readline()
                line = f.readline()
                while line:
                    key, value = line.strip().split(": ")
                    if key == "Chart":
                        chart = value
                    elif key == "Picture":
                        picture = value
                    elif key == "Song":
                        song = value
                    elif key == "Name":
                        name = value
                    elif key == "Level":
                        level = value
                    elif key == "Composer":
                        composer = value
                    elif key == "Charter":
                        charter = value
                    line = f.readline()
        except UnicodeDecodeError:
            with open("./cache/temp/info.txt", "r", encoding="utf-8") as f:
                # 第一行是 #
                f.readline()
                line = f.readline()
                while line:
                    key, value = line.strip().split(": ")
                    if key == "Chart":
                        chart = value
                    elif key == "Picture":
                        picture = value
                    elif key == "Song":
                        song = value
                    elif key == "Name":
                        name = value
                    elif key == "Level":
                        level = value
                    elif key == "Composer":
                        composer = value
                    elif key == "Charter":
                        charter = value
                    line = f.readline()

    elif os.path.exists("./cache/temp/info.csv"):
        try:
            with open("./cache/temp/info.csv", "r", encoding="gbk") as f:
                csv_ptr = csv.reader(f)
                next(csv_ptr)
                values = next(csv_ptr)
                try:
                    values = next(csv_ptr)
                except StopIteration:
                    # 只有两行数据
                    pass
                chart = values[0]
                song = values[1]
                picture = values[2]
                name = values[6]
                level = values[7]

                illustrator = values[8]
                # rpe META 里没有曲绘师
                # 向所有曲绘师致敬

                charter = values[9]
        except UnicodeDecodeError:
            with open("./cache/temp/info.csv", "r", encoding="utf-8") as f:
                csv_ptr = csv.reader(f)
                next(csv_ptr)
                values = next(csv_ptr)
                try:
                    values = next(csv_ptr)
                except StopIteration:
                    # 只有两行数据
                    pass
                chart = values[0]
                song = values[1]
                picture = values[2]
                name = values[6]
                level = values[7]
                illustrator = values[8]
                charter = values[9]

    for file in os.listdir("./cache/temp"):
        if file.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"] and not picture:
            picture = file
        elif file.split(".")[-1].lower() in ["json", "pec", "mc"] and not chart:
            chart = file
        elif file.split(".")[-1].lower() in ["mp3", "ogg", "wav", "aac"] and not song:
            song = file
    print("done")

    print("copying files ...    ", end='')
    print("audio:", song)
    try:
        os.mkdir(f"./beatmaps/{md5}")

        try:
            shutil.copy(f"./cache/temp/{song}", f"./beatmaps/{md5}")
            shutil.copy(f"./cache/temp/{picture}", f"./beatmaps/{md5}")
        except FileNotFoundError:
            print("error")
            # 有可能是解码异常
            # 用万能找文件法再找一次
            for file in os.listdir("./cache/temp"):
                if file.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                    picture = file
                elif file.split(".")[-1].lower() in ["json", "pec", "mc"]:
                    chart = file
                elif file.split(".")[-1].lower() in ["mp3", "ogg", "wav", "aac"]:
                    song = file

            shutil.copy(f"./cache/temp/{song}", f"./beatmaps/{md5}")
            shutil.copy(f"./cache/temp/{picture}", f"./beatmaps/{md5}")

        print("done")

        print("converting chart to RPE format ...    ", end='')

        if is_rpe(f"./cache/temp/{chart}"):
            print("[RPE FORMAT DETECTED]    ", end='')
            shutil.copy(f"./cache/temp/{chart}", f"./beatmaps/{md5}")
        elif is_pec(f"./cache/temp/{chart}"):
            print("[PEC FORMAT DETECTED]    ", end='')
            pec2rpe.convert(f"./cache/temp/{chart}", f"./beatmaps/{md5}/{chart}")
        elif is_pigeon(f"./cache/temp/{chart}"):
            print("[PIGEON FORMAT DETECTED]    ", end='')
            json2rpe.convert(f"./cache/temp/{chart}", f"./beatmaps/{md5}/{chart}")
        elif is_mc(f"./cache/temp/{chart}"):
            print("[MC FORMAT DETECTED]    ", end='')
            mc2rpe.convert(f"./cache/temp/{chart}", f"./beatmaps/{md5}/{chart}")
        else:
            raise ValueError(
                f"不支持的文件类型: {chart}"
            )
        with open(f"./beatmaps/{md5}/{chart}", "r") as f:
            beatmap_json = json.load(f)

        if not beatmap_json["META"]["background"]:
            beatmap_json["META"]["background"] = picture

        if not beatmap_json["META"]["charter"]:
            beatmap_json["META"]["charter"] = charter

        if not beatmap_json["META"]["composer"]:
            beatmap_json["META"]["composer"] = composer

        if not beatmap_json["META"]["level"]:
            beatmap_json["META"]["level"] = level

        if not beatmap_json["META"]["name"]:
            beatmap_json["META"]["name"] = name

        if not beatmap_json["META"]["song"]:
            beatmap_json["META"]["song"] = song

        with open(f"./beatmaps/{md5}/{chart}", "w") as f:
            json.dump(beatmap_json, f)

        print("done")
        return 0

    except Exception as e:
        shutil.rmtree(f"./beatmaps/{md5}")
        raise e


if __name__ == '__main__':
    # load_dir("resources/56769032")
    # pec2rpe.convert("./resources/56769032/56769032.json", "dfksj")
    import_zip("../mc2phi/pupa.zip")

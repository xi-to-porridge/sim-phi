import json
import os
# import shutil


def convert(chart, dest):
    dir_path = os.path.dirname(chart)

    song = ''
    picture = ''
    # name = ''
    for file in os.listdir(dir_path):
        if file.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
            picture = file
        elif file.split(".")[-1].lower() in ["json", "mc"]:
            chart = file
            # name = '.'.join(file.split(".")[:-1])
        elif file.split(".")[-1].lower() in ["mp3", "ogg", "wav", "aac"]:
            song = file

    with open(dir_path + os.sep + chart, "r") as f:
        mc = json.load(f)

    rpe = {
        "BPMList": [],
        "META": {
            "RPEVersion": 100,
            "background": picture,
            "charter": mc["meta"]["creator"],
            "composer": mc["meta"]["song"]["artist"],
            "id": f"m.{mc['meta']['id']}",
            "level": mc["meta"]["version"],
            "name": mc["meta"]["song"]["title"],
            "offset": -mc["note"][-1]["offset"],
            "song": song
        },
        "judgeLineGroup": ["Default"],
        "judgeLineList": [
            {
                "Group": 0,
                "Name": "Untitled",
                "Texture": "line.png",
                "eventLayers": [
                    {
                        "alphaEvents": [
                            {
                                "easingType": 1,
                                "end": 255,
                                "endTime": [0, 0, 1],
                                "linkgroup": 0,
                                "start": 255,
                                "startTime": [0, 0, 1]
                            },
                        ],
                        "moveXEvents": [
                            {
                                "easingType": 1,
                                "end": 0.0,
                                "endTime": [0, 0, 1],
                                "linkgroup": 0,
                                "start": 0.0,
                                "startTime": [0, 0, 1]
                            }
                        ],
                        "moveYEvents": [
                            {
                                "easingType": 1,
                                "end": -300,
                                "endTime": [0, 0, 1],
                                "linkgroup": 0,
                                "start": -300,
                                "startTime": [0, 0, 1]
                            },
                        ],
                        "rotateEvents": [
                            {
                                "easingType": 1,
                                "end": 0.0,
                                "endTime": [0, 0, 1],
                                "linkgroup": 0,
                                "start": 0.0,
                                "startTime": [0, 0, 1]
                            }
                        ],
                        "speedEvents": [
                            {
                                "end": 10,
                                "endTime": [0, 0, 1],
                                "linkgroup": 0,
                                "start": 10,
                                "startTime": [0, 0, 1]
                            }
                        ]
                    }
                ],
                "isCover": 1,
                "notes": [],
                "numOfNotes": 0
            }
        ]
    }

    track_num = mc["meta"]["mode_ext"]["column"]
    gap = 1400 / (track_num + 1)
    start = -700
    note_num = 0
    del mc["note"][-1]  # 最后一个note是sound/vol/offset/type等信息

    for note in mc["note"]:
        note_num += 1
        if "endbeat" in note:
            rpe["judgeLineList"][0]["notes"].append(
                {
                    "above": 1,
                    "alpha": 255,
                    "endTime": note["endbeat"],
                    "isFake": 0,
                    "positionX": start + (note["column"] + 1) * gap,
                    "size": 1.0,
                    "speed": 1.0,
                    "startTime": note["beat"],
                    "type": 2,
                    "visibleTime": 999999.0,
                    "yOffset": 0.0
                },
            )
        else:
            rpe["judgeLineList"][0]["notes"].append(
                {
                    "above": 1,
                    "alpha": 255,
                    "endTime": note["beat"],
                    "isFake": 0,
                    "positionX": start + (note["column"] + 1) * gap,
                    "size": 1.0,
                    "speed": 1.0,
                    "startTime": note["beat"],
                    "type": 1,
                    "visibleTime": 999999.0,
                    "yOffset": 0.0
                },
            )

    rpe["judgeLineList"][0]["numOfNotes"] = note_num

    for beat_bpm in mc["time"]:
        beat, bpm = beat_bpm["beat"], beat_bpm["bpm"]
        rpe["BPMList"].append({
            "bpm": bpm,
            "startTime": beat
        })

    # if os.path.exists(f"./cache/m2pCache"):
    #     shutil.rmtree(f"./cache/m2pCache")

    # os.mkdir(f"./cache/m2pCache")

    # shutil.copy(dir_path + os.sep + song, f"./cache/m2pCache")
    # shutil.copy(dir_path + os.sep + picture, f"./cache/m2pCache")

    with open(dest, "w") as f:
        json.dump(rpe, f)


if __name__ == '__main__':
    import sys
    convert(sys.argv[1])
    # convert("0")

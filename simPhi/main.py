import core
import element
import scenes


def run(keyboard_offset):
    core.KEYBOARD_OFFSET = keyboard_offset
    core.MODE = "userplay"
    core.evalPainter = element.EvalPainter()
    core.scene = core.SELECT

    # ---------- GAME START ----------

    while 1:
        if core.scene == core.PLAYING or core.PAUSE:
            if core.MODE == "userplay":
                scenes.PlayingUser.render()
            else:
                scenes.PlayingAuto.render()

        elif core.scene == core.END_1:
            scenes.End1.render()

        elif core.scene == core.END_2:
            scenes.End2.render()

        elif core.scene == core.PREPARE:
            scenes.Prepare.render()

        elif core.scene == core.SELECT:
            scenes.SelectBeatmap.render()


if __name__ == '__main__':
    run(0)

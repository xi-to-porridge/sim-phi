import pygame
import sys
import core
from core import CONTINUE
import time


class Prepare:
    def __init__(self):
        self.start = -1

    def render(self):
        if self.start == -1:
            # 进入Prepare
            self.start = time.time()

        # ---------- EVENT RESPOND ----------

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                core.send_to_server("shutdown")
                sys.exit(0)

        # ---------- EVENT RESPOND ----------

        # ---------- ESSENTIAL ----------

        if time.time() - self.start > core.PREPARE_DURATION:
            core.scene = core.PLAYING
            core.surface.set_alpha(255)
            pygame.mixer.music.play()
            self.start = -1
            return CONTINUE

        core.screen.blit(core.background, (0, 0))
        # screen.fill((50, 50, 50))
        core.surface.fill((0, 0, 0, 0))

        # 进度条
        pygame.draw.rect(core.surface, (200, 200, 200),
                         (
                             (0,
                              core.HEIGHT / 2 - 3),
                             (
                                 (min(time.time() - self.start, 1)) * 8 / core.PREPARE_DURATION * core.WIDTH,
                                 6)
                         ))

        # ---------- ESSENTIAL ----------

        # ---------- DISPLAY TEXTS ----------
        passed = time.time() - self.start
        fade = 1
        if passed < fade:
            alpha = passed / fade * 255
        elif passed > core.PREPARE_DURATION - fade:
            alpha = (core.PREPARE_DURATION - passed) / fade * 255
        else:
            alpha = 255

        core.surface.blit(core.title_text, (core.WIDTH / 2 - core.title_text.get_width() / 2,
                                            core.HEIGHT / 2 - core.title_text.get_height() / 2 - 50))

        core.surface.blit(core.copyright_text, (core.WIDTH - core.copyright_text.get_width() - 5,
                                                core.HEIGHT - core.copyright_text.get_height() - 5))

        core.surface.set_alpha(alpha)

        # ---------- DISPLAY TEXTS ----------

        # ---------- REFRESH ----------

        core.screen.blit(core.surface, (0, 0))
        pygame.display.flip()
        core.clock.tick(120)

        # ---------- REFRESH ----------

import pygame
import sys
import core
from core import CONTINUE
import time


class End2:
    def __init__(self):
        self.start = -1

    def render(self):
        if self.start == -1:
            # 进入Prepare
            self.start = time.time()

        # ---------- EVENT RESPOND ----------

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                core.send_to_server("shutdown")
                sys.exit(0)

            if event.type == pygame.KEYDOWN:
                if event.scancode == 40:
                    # Enter
                    self.start = -1
                    core.scene = core.SELECT
                    pygame.mixer.music.stop()
                    return CONTINUE

        # ---------- EVENT RESPOND ----------

        # ---------- ESSENTIAL ----------

        core.screen.blit(core.background, (0, 0))
        # screen.fill((50, 50, 50))
        core.surface.fill((0, 0, 0, 0))

        # ---------- ESSENTIAL ----------

        # ---------- DISPLAY ----------

        passed = time.time() - self.start
        if passed > core.END_DURATION_2:
            core.surface.blit(core.back_text, (core.WIDTH * 0.55,
                                               core.HEIGHT * 0.73))
            passed = core.END_DURATION_2

        delta_x = core.t2x.calculate(passed)

        # 左半边
        pygame.draw.rect(core.surface, (32, 32, 32, 225), (
            core.WIDTH * 0.05 - delta_x - 5, core.HEIGHT * 0.15 - 5,
            core.WIDTH * 0.4 + 10, core.HEIGHT * 0.5 + 10,
        ))
        core.surface.blit(core.background_clear, (core.WIDTH * 0.05 - delta_x, core.HEIGHT * 0.15))

        # 右半边
        pygame.draw.rect(core.surface, (32, 32, 32, 225), (
            core.WIDTH * 0.55 + delta_x - 5, core.HEIGHT * 0.15 - 5,
            core.WIDTH * 0.4 + 10, core.HEIGHT * 0.3 + 10,
        ))

        pygame.draw.rect(core.surface, (32, 32, 32, 225), (
            core.WIDTH * 0.55 + delta_x - 5, core.HEIGHT * 0.5 - 5,
            core.WIDTH * 0.4 + 10, core.HEIGHT * 0.15 + 10,
        ))

        core.surface.blit(core.phi_image, (
            core.WIDTH * 0.57 + delta_x - 5, core.HEIGHT * 0.3 - 5 - core.phi_image.get_height() / 2
        ))

        core.surface.blit(core.score_text, (
            core.WIDTH * 0.57 + delta_x + 8 + core.phi_image.get_width(),
            core.HEIGHT * 0.3 - 5 - core.score_text.get_height() / 2 - 10
        ))

        core.surface.blit(core.max_combo_text, (
            core.WIDTH * 0.57 + delta_x + 8 + core.phi_image.get_width(),
            core.HEIGHT * 0.3 - 5 - core.max_combo_text.get_height() / 2 + 25
        ))

        core.surface.blit(core.perfect_text, (
            core.WIDTH * 0.55 + delta_x - 5 - 10 + (core.WIDTH * 0.4 + 10) * 0.2 - core.perfect_text.get_width() / 2,
            core.HEIGHT * 0.545 - 5 - core.perfect_text.get_height() / 2
        ))

        core.surface.blit(core.good_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.4 - core.good_text.get_width() / 2,
            core.HEIGHT * 0.545 - 5 - core.good_text.get_height() / 2
        ))

        core.surface.blit(core.bad_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.6 - core.bad_text.get_width() / 2,
            core.HEIGHT * 0.545 - 5 - core.bad_text.get_height() / 2
        ))

        core.surface.blit(core.miss_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.8 - core.miss_text.get_width() / 2,
            core.HEIGHT * 0.545 - 5 - core.miss_text.get_height() / 2
        ))

        core.surface.blit(core.perfect_num_text, (
            core.WIDTH * 0.55 + delta_x - 5 - 10 + (core.WIDTH * 0.4 + 10) *
            0.2 - core.perfect_num_text.get_width() / 2,
            core.HEIGHT * 0.615 - 5 - core.perfect_num_text.get_height() / 2
        ))

        core.surface.blit(core.good_num_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.4 - core.good_num_text.get_width() / 2,
            core.HEIGHT * 0.615 - 5 - core.good_num_text.get_height() / 2
        ))

        core.surface.blit(core.bad_num_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.6 - core.bad_num_text.get_width() / 2,
            core.HEIGHT * 0.615 - 5 - core.bad_num_text.get_height() / 2
        ))

        core.surface.blit(core.miss_num_text, (
            core.WIDTH * 0.55 + delta_x - 5 + (core.WIDTH * 0.4 + 10) * 0.8 - core.miss_num_text.get_width() / 2,
            core.HEIGHT * 0.615 - 5 - core.miss_num_text.get_height() / 2
        ))

        core.title_text = core.font30.render(core.NAME, True, (200, 200, 200))
        core.surface.blit(core.title_text, (
            core.WIDTH * 0.05 - delta_x, core.HEIGHT * 0.73
        ))

        core.level_text = core.font25.render(core.LEVEL, True, (200, 200, 200))
        core.surface.blit(core.level_text, (
            core.WIDTH * 0.05 - delta_x, core.HEIGHT * 0.73 + core.level_text.get_height() + 10
        ))

        core.surface.blit(core.copyright_text,
                          (core.WIDTH - core.copyright_text.get_width() - 5,
                           core.HEIGHT - core.copyright_text.get_height() - 5))


        # ---------- DISPLAY ----------

        # ---------- REFRESH ----------

        core.screen.blit(core.surface, (0, 0))
        pygame.display.flip()
        core.clock.tick(120)

        # ---------- REFRESH ----------

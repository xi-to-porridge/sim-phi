import os
import pygame
import sys
import core
from core import CONTINUE
import time
import easing


class End1:
    def __init__(self):
        self.start = -1

    def render(self):
        if self.start == -1:
            # 进入Prepare
            self.start = time.time()

        # ---------- EVENT RESPOND ----------

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                core.send_to_server("shutdown")
                sys.exit(0)

        # ---------- EVENT RESPOND ----------

        # ---------- ESSENTIAL ----------

        if time.time() - self.start > core.END_DURATION_1:
            core.scene = core.END_2
            core.background_clear = pygame.transform.scale(
                pygame.image.load(core.selected_beatmap + os.sep + core.IMAGE),
                (core.WIDTH * 0.4, core.HEIGHT * 0.5)
            )
            core.t2x = easing.code2FuncDict[4](0, core.END_DURATION_2, core.WIDTH / 2, 0)
            if core.MODE == "autoplay":
                core.max_combo_text = core.font20.render(f"AUTOPLAY", True, (240, 10, 100))
            else:
                if core.perfect_num == core.NOTE_NUM:
                    core.max_combo_text = core.font20.render(f"PERFECT", True, (240, 10, 100))
                elif core.max_combo == core.NOTE_NUM:
                    core.max_combo_text = core.font20.render(f"FULL COMBO", True, (240, 10, 100))
                else:
                    core.max_combo_text = core.font20.render(f"Max Combo: {core.max_combo}", True, (255, 255, 255))

            core.perfect_text = core.font20.render("Perfect", True, (255, 255, 255))
            core.good_text = core.font20.render("Good", True, (255, 255, 255))
            core.bad_text = core.font20.render("Bad", True, (255, 255, 255))
            core.miss_text = core.font20.render("Miss", True, (255, 255, 255))

            if core.MODE == "autoplay":
                core.phi_image = pygame.image.load("./resources/texture/phi.png")
                core.score_text = core.font40.render("1000000", True, (255, 255, 255))
                core.perfect_num_text = core.font20.render(f"{core.combo}", True, (255, 255, 255))
                core.good_num_text = core.font20.render("0", True, (255, 255, 255))
                core.bad_num_text = core.font20.render("0", True, (255, 255, 255))
                core.miss_num_text = core.font20.render("0", True, (255, 255, 255))
            else:
                # core.send_to_server("ei ")
                if core.score == 1000000:
                    img_name = "phi"
                elif core.score >= 950000:
                    if core.NOTE_NUM == core.combo:
                        img_name = "vfc"
                    else:
                        img_name = "v"
                elif core.score >= 920000:
                    img_name = "s"
                elif core.score >= 880000:
                    img_name = "a"
                elif core.score >= 820000:
                    img_name = "b"
                elif core.score >= 700000:
                    img_name = "c"
                else:
                    img_name = "f"

                core.phi_image = pygame.image.load(f"./resources/texture/{img_name}.png")
                core.score_text = core.font40.render(f"{core.score}".rjust(7, "0"), True, (255, 255, 255))
                core.max_combo_text = core.font20.render(f"max combo: {core.max_combo}", True, (255, 255, 255))

                core.perfect_num_text = core.font20.render(f"{core.perfect_num}", True, (255, 255, 255))
                core.good_num_text = core.font20.render(f"{core.good_num}", True, (255, 255, 255))
                core.bad_num_text = core.font20.render(f"{core.bad_num}", True, (255, 255, 255))
                core.miss_num_text = core.font20.render(f"{core.miss_num}", True, (255, 255, 255))

            pygame.mixer.music.load("./resources/audio/GameOver.wav")
            pygame.mixer.music.play(-1, fade_ms=200)
            self.start = -1
            return CONTINUE

        core.screen.blit(core.background, (0, 0))
        # screen.fill((50, 50, 50))
        core.surface.fill((0, 0, 0, 0))

        # ---------- ESSENTIAL ----------

        # ---------- DISPLAY TEXTS ----------

        passed = time.time() - self.start
        delta_y = 90 * passed / 1

        combo_text = core.font30.render("COMBO", True, (255, 255, 255))
        combo_num_text = core.font40.render(str(core.combo), True, (255, 255, 255))
        score_text = core.font30.render(str(core.score).rjust(7, '0'), True,
                                        (255, 255, 255))

        fps_text = core.font25.render(str(int(core.clock.get_fps())).rjust(3, "0"), True, (255, 255, 255))
        offset_text = core.font20.render(f"OFFSET={core.OFFSET}", True, (255, 255, 255))

        if core.combo >= 3:
            core.surface.blit(combo_text, (core.WIDTH / 2 - combo_text.get_width() / 2, 40 - delta_y))
            core.surface.blit(combo_num_text, (core.WIDTH / 2 - combo_num_text.get_width() / 2, 0 - delta_y))

        core.surface.blit(score_text, (core.WIDTH - score_text.get_width(), 0 - delta_y))
        core.surface.blit(fps_text, (0, 0 - delta_y))
        core.surface.blit(offset_text, (0, fps_text.get_height() - delta_y))

        core.surface.blit(core.name_text, (5,
                                           core.HEIGHT - core.name_text.get_height() - 5 + delta_y))

        core.surface.blit(core.level_text, (core.WIDTH - core.level_text.get_width() - 5,
                                            core.HEIGHT - core.level_text.get_height() -
                                            core.copyright_text.get_height() - 5 + delta_y))

        core.surface.blit(core.copyright_text, (core.WIDTH - core.copyright_text.get_width() - 5,
                                                core.HEIGHT - core.copyright_text.get_height() - 5 + delta_y))

        # ---------- DISPLAY TEXTS ----------

        # ---------- REFRESH ----------

        core.screen.blit(core.surface, (0, 0))
        pygame.display.flip()
        core.clock.tick(120)

        # ---------- REFRESH ----------

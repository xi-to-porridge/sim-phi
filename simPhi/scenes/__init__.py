from . import end_1, end_2, prepare, playing_auto, select_beatmap, playing_user


End1 = end_1.End1()
End2 = end_2.End2()
Prepare = prepare.Prepare()
PlayingAuto = playing_auto.PLAYING()
SelectBeatmap = select_beatmap.SelectBeatmap()
PlayingUser = playing_user.PLAYING()

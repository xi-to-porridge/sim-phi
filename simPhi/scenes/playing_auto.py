import pygame
import sys
import core
from core import CONTINUE
import element
from PIL import Image, ImageFilter


class PLAYING:
    def __init__(self):
        pass

    def render(self):
        # ---------- EVENT RESPOND ----------

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                core.send_to_server("shutdown")
                sys.exit(0)
            elif event.type == pygame.MOUSEWHEEL:
                core.OFFSET += event.y * 25
            elif event.type == pygame.KEYDOWN:
                if event.scancode == 41:
                    # Esc
                    core.PAUSE = not core.PAUSE

                    if core.PAUSE:
                        pygame.mixer.music.pause()
                        # pygame.image.save(core.screen, "./cache/shot.jpg")

                        # pil_blurred = Image.open("./cache/shot.jpg").filter(ImageFilter.GaussianBlur(radius=15))
                        pil_blurred = Image.frombytes("RGB", core.screen.get_size(), pygame.image.tostring(
                            core.screen, "RGB"
                        )).filter(ImageFilter.GaussianBlur(radius=15))
                        # bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                        # img = bright_enhancer.enhance(0.5)
                        # pil_blurred.convert("RGB").save("./cache/shot.jpg")

                        core.pause_surface = pygame.image.fromstring(pil_blurred.tobytes(),
                                                                     core.screen.get_size(), "RGB")

                    else:
                        pygame.mixer.music.unpause()
                elif event.scancode == 20:
                    # Q
                    core.scene = core.SELECT
                    core.PAUSE = False
                    return CONTINUE

        # ---------- EVENT RESPOND ----------

        # ---------- ESSENTIAL ----------
        if not pygame.mixer.music.get_busy() and not core.PAUSE:
            core.scene = core.END_1
            return CONTINUE

        if core.beat < 0:
            core.beat = core.BeatObject.get_value((pygame.mixer.music.get_pos() / 1000 - core.OFFSET / 1000) / 60)
            return CONTINUE

        core.screen.blit(core.background, (0, 0))
        # core.screen.fill((50, 50, 50))
        core.surface.fill((0, 0, 0, 0))

        # 进度条
        pygame.draw.rect(core.screen, (200, 200, 200),
                         ((0, 0), ((pygame.mixer.music.get_pos() / 1000) / core.DURATION * core.WIDTH, 8)))

        # ---------- ESSENTIAL ----------

        # ---------- DISPLAY ELEMENTS ----------
        core.send_to_server(f"gc {core.beat}")
        respond = core.read_server()[:-2]

        if respond:
            respond = respond.split("|")

            for block in respond:
                x1, y1, x2, y2, alpha = map(float, block.split(" "))
                pygame.draw.line(core.surface, (255, 255, 255, alpha), (x1, y1), (x2, y2), width=5)

        respond = core.read_server()[:-2]

        if respond:
            respond = respond.split("|")
            for block in respond:
                # print(c.Fore.CYAN, block)
                if block[0] != "4":
                    _, x1, y1, x2, y2, x3, y3, x4, y4, r, g, b, alpha = map(float, block.split(" "))
                    pygame.draw.polygon(core.surface,
                                        (r, g, b, alpha), [
                                            (x1, y1),
                                            (x2, y2),
                                            (x3, y3),
                                            (x4, y4),
                                        ])
                elif block[0] == "4":
                    _, x1, y1, x2, y2, x3, y3, x4, y4, x5, x6, x7, x8, y5, y6, y7, y8, r, g, b, alpha = \
                        map(float, block.split(" "))

                    pygame.draw.polygon(core.surface,
                                        (r, g, b, alpha), [
                                            (x1, y1),
                                            (x2, y2),
                                            (x3, y3),
                                            (x4, y4),
                                        ])
                    pygame.draw.polygon(core.surface,
                                        (r, g, b, alpha), [
                                            (x5, y5),
                                            (x6, y6),
                                            (x7, y7),
                                            (x8, y8),
                                        ])

        respond = core.read_server()[:-2]
        # print(c.Fore.GREEN, respond)
        if respond:
            respond = respond.split("|")
            for block in respond:
                x, y = map(float, block.split(' '))
                core.evalPainter.add_note(x, y, core.Eval.PERFECT)

        respond = core.read_server()[:-2]
        # print(c.Fore.YELLOW, respond)
        if respond and core.ENABLE_SOUND:
            respond = respond.split("|")
            for block in respond:
                _type = int(block)
                if _type == element.Note.TAP:
                    core.TAP_SOUND.play()
                elif _type == element.Note.DRAG:
                    core.DRAG_SOUND.play()
                elif _type == element.Note.FLICK:
                    core.FLICK_SOUND.play()
                else:
                    core.TAP_SOUND.play()
                # core.note_num += 1
                # TODO: 删掉上面这个语句

        respond = core.read_server()[:-2]

        if respond:
            respond = respond.split("|")
            core.score, \
                core.perfect_num, core.good_num, core.bad_num, core.miss_num, \
                core.combo, core.max_combo = map(int, respond)

        core.evalPainter.blit(core.surface)

        # ---------- DISPLAY ELEMENTS ----------

        # ---------- DISPLAY TEXTS ----------

        combo_text = core.font30.render("COMBO", True, (255, 255, 255))
        combo_num_text = core.font40.render(str(core.combo), True, (255, 255, 255))
        score_text = core.font30.render(str(core.score).rjust(7, '0'), True, (255, 255, 255))
        fps_text = core.font25.render(str(int(core.clock.get_fps())).rjust(3, "0"), True, (255, 255, 255))
        offset_text = core.font20.render(f"OFFSET={core.OFFSET}", True, (255, 255, 255))

        if core.combo >= 3:
            core.surface.blit(combo_text, (core.WIDTH / 2 - combo_text.get_width() / 2, 40))
            core.surface.blit(combo_num_text, (core.WIDTH / 2 - combo_num_text.get_width() / 2, 0))

        core.surface.blit(score_text, (core.WIDTH - score_text.get_width(), 0))
        core.surface.blit(fps_text, (0, 0))
        core.surface.blit(offset_text, (0, fps_text.get_height()))

        core.surface.blit(core.name_text, (5,
                                           core.HEIGHT - core.name_text.get_height() - 5))

        core.surface.blit(core.level_text, (core.WIDTH - core.level_text.get_width() - 5,
                                            core.HEIGHT - core.level_text.get_height() -
                                            core.copyright_text.get_height() - 5))

        core.surface.blit(core.copyright_text, (core.WIDTH - core.copyright_text.get_width() - 5,
                                                core.HEIGHT - core.copyright_text.get_height() - 5))

        # ---------- DISPLAY TEXTS ----------

        # ---------- REFRESH ----------

        if core.PAUSE:

            # core.surface.blit(core.Texture[core.Texture.GRAY_BG], (0, 0))
            # print(core.Texture[core.Texture.GRAY_BG].get_width())
            core.screen.blit(core.pause_surface, (0, 0))
            core.screen.blit(core.unpause_text_1,
                             (core.WIDTH / 2 - core.unpause_text_1.get_width() / 2,
                              core.HEIGHT / 2 - core.unpause_text_1.get_height() / 2 - 10-20))
            core.screen.blit(core.unpause_text_2,
                             (core.WIDTH / 2 - core.unpause_text_2.get_width() / 2,
                              core.HEIGHT / 2 - core.unpause_text_2.get_height() / 2 - 10+20))
        else:
            core.screen.blit(core.surface, (0, 0))
        pygame.display.flip()
        core.clock.tick(120)
        # beat = core.BeatObject.get_value(skip/60)
        core.beat = core.BeatObject.get_value((pygame.mixer.music.get_pos() / 1000 - core.OFFSET / 1000) / 60)

        # ---------- REFRESH ----------

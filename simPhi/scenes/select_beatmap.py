import shutil
import traceback

import pygame
import sys
import core
import data
import tinytag
from PIL import Image, ImageFilter, ImageEnhance
import os
from core import CONTINUE
import json
import easygui


class SelectBeatmap:
    def __init__(self):
        self.shadow = pygame.Surface((core.select_import_text1.get_width()+10,
                                     core.select_import_text1.get_height()*3+10),
                                     pygame.SRCALPHA)
        self.shadow.fill((100, 100, 100, 128))
        self.shadow.set_alpha(128)


    def render(self):

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                core.send_to_server("shutdown")
                sys.exit(0)
            elif event.type == pygame.KEYDOWN:
                if event.scancode == 80:
                    core.surface.fill((128, 128, 128, 100))
                    core.surface.blit(core.select_wait_text, (core.WIDTH / 2 - core.select_wait_text.get_width() / 2,
                                                              core.HEIGHT / 2 - core.select_wait_text.get_height() / 2))
                    core.screen.blit(core.surface, (0, 0))
                    pygame.display.flip()

                    core.beatmap_index += 1
                    core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]

                    for file_name in os.listdir(core.selected_beatmap):
                        if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                            # background = pygame.image.load(f"cache/{file_name}")

                            pil_blurred = Image.open(f"{core.selected_beatmap}/{file_name}").convert("RGB").filter(
                                ImageFilter.GaussianBlur(radius=15))
                            bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                            pil_blurred = bright_enhancer.enhance(0.5)

                            core.background = pygame.transform.smoothscale(
                                pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                                (core.WIDTH, core.HEIGHT)
                            )
                            break
                            # # img = Image.f
                            # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

                    for file_name in os.listdir(core.selected_beatmap):
                        if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
                            fp = open(f"{core.selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
                            chart_json = json.load(fp)
                            fp.close()
                            # 加载基本信息
                            core.NAME = chart_json["META"]["name"]
                            core.COMPOSER = chart_json["META"]["composer"]
                            core.CHARTER = chart_json["META"]["charter"]
                            core.LEVEL = chart_json["META"]["level"]

                            core.select_title = core.font50.render(core.NAME, True, (255, 255, 255), (128, 128, 128, 128)
                                                                   )
                            core.composer_text = core.font30.render("Composer: " + core.COMPOSER, True, (255, 255, 255),
                                                                    )
                            core.charter_text = core.font30.render("Charter: " + core.CHARTER, True, (255, 255, 255),
                                                                   )
                            core.level_text = core.font30.render(core.LEVEL, True, (255, 255, 255), )

                            break

                elif event.scancode == 79:
                    core.surface.fill((128, 128, 128, 100))
                    core.surface.blit(core.select_wait_text, (core.WIDTH / 2 - core.select_wait_text.get_width() / 2,
                                                              core.HEIGHT / 2 - core.select_wait_text.get_height() / 2))
                    core.screen.blit(core.surface, (0, 0))
                    pygame.display.flip()

                    core.beatmap_index -= 1
                    core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]

                    for file_name in os.listdir(core.selected_beatmap):
                        if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                            # background = pygame.image.load(f"cache/{file_name}")

                            pil_blurred = Image.open(f"{core.selected_beatmap}/{file_name}").convert("RGB").filter(
                                ImageFilter.GaussianBlur(radius=15))
                            bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                            pil_blurred = bright_enhancer.enhance(0.5)
                            # pil_blurred.convert("RGB").save("./cache/shot.jpg")

                            core.background = pygame.transform.smoothscale(
                                pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                                (core.WIDTH, core.HEIGHT)
                            )
                            break
                            # # img = Image.f
                            # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

                    for file_name in os.listdir(core.selected_beatmap):
                        if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
                            fp = open(f"{core.selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
                            chart_json = json.load(fp)
                            fp.close()
                            # 加载基本信息
                            core.NAME = chart_json["META"]["name"]
                            core.COMPOSER = chart_json["META"]["composer"]
                            core.CHARTER = chart_json["META"]["charter"]
                            core.LEVEL = chart_json["META"]["level"]

                            core.select_title = core.font50.render(core.NAME, True, (255, 255, 255), (128, 128, 128, 128)
                                                                   )
                            core.composer_text = core.font30.render("Composer: " + core.COMPOSER, True, (255, 255, 255),
                                                                    )
                            core.charter_text = core.font30.render("Charter: " + core.CHARTER, True, (255, 255, 255),
                                                                   )
                            core.level_text = core.font30.render(core.LEVEL, True, (255, 255, 255), )

                            break

                elif event.scancode == 40:
                    # enter
                    core.surface.fill((128, 128, 128, 100))
                    core.surface.blit(core.select_wait_text, (core.WIDTH / 2 - core.select_wait_text.get_width() / 2,
                                                              core.HEIGHT / 2 - core.select_wait_text.get_height() / 2))
                    core.screen.blit(core.surface, (0, 0))
                    pygame.display.flip()
                    data.load_beatmap(os.path.basename(core.selected_beatmap))
                    core.name_text = core.font25.render("| " + core.NAME, True, (255, 255, 255))
                    core.level_text = core.font25.render(core.LEVEL, True, (255, 255, 255))
                    core.title_text = core.font40.render(core.NAME, True, (255, 255, 255))
                    # core.title_text = core.font40.render(core.NAME, True, (255, 255, 255))
                    pygame.mixer.music.load(core.selected_beatmap + os.sep + core.SONG)
                    core.DURATION = tinytag.TinyTag.get(core.selected_beatmap + os.sep + core.SONG).duration
                    core.beat = core.BeatObject.get_value((-core.OFFSET / 1000) / 60)

                    core.send_to_server(f"gc {core.beat}")
                    respond = core.read_server()[:-2]
                    respond = core.read_server()[:-2]
                    respond = core.read_server()[:-2]
                    respond = core.read_server()[:-2]
                    if core.MODE == "userplay":
                        respond = core.read_server()[:-2]
                    respond = core.read_server()[:-2]
                    respond = respond.split("|")
                    core.score, \
                    core.perfect_num, core.good_num, core.bad_num, core.miss_num, core.combo, core.max_combo = map(
                        int,
                        respond)
                    core.scene = core.PREPARE
                    return CONTINUE
                elif event.scancode == 82:
                    # up
                    if core.MODE == "autoplay":
                        core.MODE = "userplay"
                    else:
                        core.MODE = "autoplay"
                elif event.scancode == 81:
                    # down
                    if core.MODE == "autoplay":
                        core.MODE = "userplay"
                    else:
                        core.MODE = "autoplay"
                elif event.scancode == 12:
                    # i
                    new = False
                    files = easygui.fileopenbox("选择zip格式的铺面文件", "导入铺面", multiple=True)
                    if not files:
                        continue
                    for file in files:
                        core.screen.fill((0, 0, 0))
                        core.surface.fill((128, 128, 128, 100))
                        path_text = core.font20.render("正在导入: " + file, True, (255, 255, 255))
                        core.surface.blit(core.select_importing_text,
                                          (core.WIDTH / 2 - core.select_importing_text.get_width() / 2,
                                           core.HEIGHT / 2 - core.select_importing_text.get_height() / 2 - 15))
                        core.surface.blit(path_text,
                                          (core.WIDTH / 2 - path_text.get_width() / 2,
                                           core.HEIGHT / 2 - path_text.get_height() / 2 + 20))
                        core.screen.blit(core.surface, (0, 0))
                        pygame.display.flip()
                        try:
                            data.import_zip(file)
                            new = True
                        except:
                            print(f"WARNING: {file} is not an illegal beatmap zip file")
                            print(traceback.format_exc())

                    if new:
                        if not os.path.exists("./beatmaps"):
                            os.mkdir("./beatmaps")
                        core._beatmaps = os.listdir("./beatmaps")
                        core.beatmaps = []
                        core.beatmap_index = 0

                        for core._beatmap in core._beatmaps:
                            core._beatmap = "./beatmaps/" + core._beatmap
                            if os.path.isdir(core._beatmap):
                                core.beatmaps.append(core._beatmap)

                        if core.beatmaps:
                            core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]
                        else:
                            core.selected_beatmap = ''

                        for file_name in os.listdir(core.selected_beatmap):
                            if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                                # background = pygame.image.load(f"cache/{file_name}")

                                pil_blurred = Image.open(f"{core.selected_beatmap}/{file_name}").convert("RGB").filter(
                                    ImageFilter.GaussianBlur(radius=15))
                                bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                                pil_blurred = bright_enhancer.enhance(0.5)

                                core.background = pygame.transform.smoothscale(
                                    pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                                    (core.WIDTH, core.HEIGHT)
                                )
                                break
                                # # img = Image.f
                                # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

                        for file_name in os.listdir(core.selected_beatmap):
                            if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
                                fp = open(f"{core.selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
                                chart_json = json.load(fp)
                                fp.close()
                                # 加载基本信息
                                core.NAME = chart_json["META"]["name"]
                                core.COMPOSER = chart_json["META"]["composer"]
                                core.CHARTER = chart_json["META"]["charter"]
                                core.LEVEL = chart_json["META"]["level"]

                                core.select_title = core.font50.render(core.NAME,
                                                                       True, (255, 255, 255), (100, 100, 100, 100))
                                core.composer_text = core.font30.render("Composer: " + core.COMPOSER,
                                                                        True, (255, 255, 255))
                                core.charter_text = core.font30.render("Charter: " + core.CHARTER,
                                                                       True, (255, 255, 255))
                                core.level_text = core.font30.render(core.LEVEL,
                                                                     True, (255, 255, 255))
                                break
                elif event.scancode == 7:
                    # d
                    shutil.rmtree(core.selected_beatmap)
                    if not os.path.exists("./beatmaps"):
                        os.mkdir("./beatmaps")
                    core._beatmaps = os.listdir("./beatmaps")
                    core.beatmaps = []
                    core.beatmap_index = 0

                    for core._beatmap in core._beatmaps:
                        core._beatmap = "./beatmaps/" + core._beatmap
                        if os.path.isdir(core._beatmap):
                            core.beatmaps.append(core._beatmap)

                    if core.beatmaps:
                        core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]
                    else:
                        core.selected_beatmap = ''

                    if core.selected_beatmap:
                        for file_name in os.listdir(core.selected_beatmap):
                            if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                                # background = pygame.image.load(f"cache/{file_name}")

                                pil_blurred = Image.open(f"{core.selected_beatmap}/{file_name}").convert("RGB").filter(
                                    ImageFilter.GaussianBlur(radius=15))
                                bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                                pil_blurred = bright_enhancer.enhance(0.5)

                                core.background = pygame.transform.smoothscale(
                                    pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                                    (core.WIDTH, core.HEIGHT)
                                )
                                break
                                # # img = Image.f
                                # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

                        for file_name in os.listdir(core.selected_beatmap):
                            if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
                                fp = open(f"{core.selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
                                chart_json = json.load(fp)
                                fp.close()
                                # 加载基本信息
                                core.NAME = chart_json["META"]["name"]
                                core.COMPOSER = chart_json["META"]["composer"]
                                core.CHARTER = chart_json["META"]["charter"]
                                core.LEVEL = chart_json["META"]["level"]

                                core.select_title = core.font50.render(core.NAME,
                                                                       True, (255, 255, 255), (100, 100, 100, 100))
                                core.composer_text = core.font30.render("Composer: " + core.COMPOSER,
                                                                        True, (255, 255, 255))
                                core.charter_text = core.font30.render("Charter: " + core.CHARTER,
                                                                       True, (255, 255, 255))
                                core.level_text = core.font30.render(core.LEVEL,
                                                                     True, (255, 255, 255))
                                break
            elif event.type == pygame.MOUSEWHEEL:
                core.beatmap_index -= event.y
                core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]
                core.surface.fill((128, 128, 128, 100))
                core.surface.blit(core.select_wait_text, (core.WIDTH / 2 - core.select_wait_text.get_width() / 2,
                                                          core.HEIGHT / 2 - core.select_wait_text.get_height() / 2))
                core.screen.blit(core.surface, (0, 0))
                pygame.display.flip()

                core.beatmap_index += 1
                core.selected_beatmap = core.beatmaps[core.beatmap_index % len(core.beatmaps)]

                for file_name in os.listdir(core.selected_beatmap):
                    if file_name.split(".")[-1].lower() in ["jpg", "jpeg", "png", "bmp"]:
                        # background = pygame.image.load(f"cache/{file_name}")

                        pil_blurred = Image.open(f"{core.selected_beatmap}/{file_name}").convert("RGB").filter(
                            ImageFilter.GaussianBlur(radius=15))
                        bright_enhancer = ImageEnhance.Brightness(pil_blurred)
                        pil_blurred = bright_enhancer.enhance(0.5)

                        core.background = pygame.transform.smoothscale(
                            pygame.image.fromstring(pil_blurred.tobytes(), pil_blurred.size, "RGB"),
                            (core.WIDTH, core.HEIGHT)
                        )
                        break
                        # # img = Image.f
                        # background = pygame.image.frombuffer(archive.read(file_name), (100, 100), "RGB")

                for file_name in os.listdir(core.selected_beatmap):
                    if file_name.split(".")[-1].lower() in ["json", "pec", "mc"]:
                        fp = open(f"{core.selected_beatmap}/{file_name}", 'r', encoding="UTF-8")
                        chart_json = json.load(fp)
                        fp.close()
                        # 加载基本信息
                        core.NAME = chart_json["META"]["name"]
                        core.COMPOSER = chart_json["META"]["composer"]
                        core.CHARTER = chart_json["META"]["charter"]
                        core.LEVEL = chart_json["META"]["level"]

                        core.select_title = core.font50.render(core.NAME, True, (255, 255, 255), (128, 128, 128, 128)
                                                               )
                        core.composer_text = core.font30.render("Composer: " + core.COMPOSER, True, (255, 255, 255),
                                                                )
                        core.charter_text = core.font30.render("Charter: " + core.CHARTER, True, (255, 255, 255),
                                                               )
                        core.level_text = core.font30.render(core.LEVEL, True, (255, 255, 255), )

                        break

        core.screen.fill((0, 0, 0))
        if core.selected_beatmap:
            if core.background is not None:
                core.surface.blit(core.background, (0, 0))
                core.surface.blit(core.select_title, (20 + 80 - 50,
                                                      core.HEIGHT - core.select_title.get_height() - 100))
            if core.MODE == "autoplay":
                pygame.draw.rect(core.surface, (100, 100, 100, 150), (20, 20, 160, 25))
                core.surface.blit(core.font20.render("AUTOPLAY", True, (255, 255, 255)), (20 + 80 - 50, 30 - 10))
                core.surface.blit(core.font20.render("YOURPLAY", True, (200, 200, 200)),
                                  (20 + 80 - 50, 30 - 10 + 25))
            else:
                pygame.draw.rect(core.surface, (100, 100, 100, 150), (20, 45, 160, 25))
                core.surface.blit(core.font20.render("AUTOPLAY", True, (200, 200, 200)), (20 + 80 - 50, 30 - 10))
                core.surface.blit(core.font20.render("YOURPLAY", True, (255, 255, 255)),
                                  (20 + 80 - 50, 30 - 10 + 25))

            core.surface.blit(core.charter_text, (20 + 80 - 50, 30 - 10 + 100))
            core.surface.blit(core.composer_text, (20 + 80 - 50, 30 - 10 + 140))
            core.surface.blit(core.level_text, (20 + 80 - 50, 30 - 10 + 180))
        else:
            core.surface.fill((128, 128, 128, 100))

        #         #             pygame.draw.rect(core.surface, (200, 200, 200, 50),
        #         #                              (core.WIDTH - 10 - core.select_import_text1.get_width()-5,
        #         #                               5,
        #         #                               core.select_import_text1.get_width()+10,
        #         #                               core.select_import_text1.get_height()*3+10))
        core.surface.blit(self.shadow, (core.WIDTH - 10 - core.select_import_text1.get_width()-5, 5))

        core.surface.blit(core.select_import_text1, (core.WIDTH - 10 - core.select_import_text1.get_width(),
                                                     10))
        core.surface.blit(core.select_import_text2, (core.WIDTH - 10 - core.select_import_text2.get_width(),
                                                     30))
        core.surface.blit(core.select_import_text3, (core.WIDTH - 10 - core.select_import_text3.get_width(),
                                                     50))

        core.surface.blit(core.copyright_text, (core.WIDTH - core.copyright_text.get_width() - 5,
                                                core.HEIGHT - core.copyright_text.get_height() - 5))
        core.screen.blit(core.surface, (0, 0))
        pygame.display.flip()

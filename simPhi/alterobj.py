import core
import easing


def list2beat(_list):
    return _list[0] + _list[1] / _list[2]


class LineXObject:
    def __init__(self, events_json):
        scale = core.LINE_X_SCALE

        for event in events_json:
            string = f"{event['easingType']} " \
                     f"{event['end'] * scale + core.WIDTH / 2} " \
                     f"{event['start'] * scale + core.WIDTH / 2} " \
                     f"{list2beat(event['endTime'])} " \
                     f"{list2beat(event['startTime'])}"
            core.send_to_server("jx " + string)


class AlphaObject:
    def __init__(self, events_json):
        for event in events_json:
            string = f"{event['easingType']} " \
                     f"{event['end']} " \
                     f"{event['start']} " \
                     f"{list2beat(event['endTime'])} " \
                     f"{list2beat(event['startTime'])}"

            core.send_to_server("ja " + string)


class LineYObject:
    def __init__(self, events_json):
        scale = core.LINE_Y_SCALE

        for event in events_json:
            string = f"{event['easingType']} " \
                     f"{event['end'] * scale + core.HEIGHT / 2} " \
                     f"{event['start'] * scale + core.HEIGHT / 2} " \
                     f"{list2beat(event['endTime'])} " \
                     f"{list2beat(event['startTime'])}"

            core.send_to_server("jy " + string)


class AngleObject:
    def __init__(self, events_json):
        for event in events_json:
            string = f"{event['easingType']} " \
                     f"{-event['end']} " \
                     f"{-event['start']} " \
                     f"{list2beat(event['endTime'])} " \
                     f"{list2beat(event['startTime'])}"

            core.send_to_server("jd " + string)


class BeatObject:
    """
    将以秒为单位的时间转换为Beat
    """

    def __init__(self, events_json):
        # TODO: 支持变速
        self.value = 0

        # 事件列表，存储了self.value的变化事件
        self.events = []

        self.events.append(
            easing.code2FuncDict[1](
                0, core.DURATION, 0, core.DURATION * events_json[0]["bpm"]
            )
        )

    def get_value(self, beat):
        # 根据self.events, 更新self.value,然后返回self.value

        for event in self.events:
            if event.start_beat > beat:
                # 列表排序过了，后面的events都还没开始呢
                break

            # 代码执行到这里，说明这个event需要处理
            if event.end_beat < beat:
                self.value = event.target
            else:
                self.value = event.calculate(beat)

        return self.value


class NoteYObject:
    def __init__(self, events_json):
        scale = core.SPEED_SCALE

        tmp_value = 0
        index = 0
        for event in events_json[:-1]:
            target = tmp_value + event["start"] * (
                    list2beat(events_json[index + 1]["startTime"]) - list2beat(event["startTime"])) * scale

            string = f"1 " \
                     f"{target} " \
                     f"{tmp_value} " \
                     f"{list2beat(events_json[index + 1]['startTime'])} " \
                     f"{list2beat(event['startTime'])}"

            core.send_to_server("jny " + string)

            index += 1
            tmp_value = target

        target = tmp_value + (
                core.DURATION - list2beat(events_json[-1]['startTime'])) * events_json[-1]['start'] * scale

        string = f"1 " \
                 f"{target} " \
                 f"{tmp_value} " \
                 f"{core.DURATION} " \
                 f"{list2beat(events_json[-1]['startTime'])}"

        core.send_to_server("jny " + string)

import pygame


class Scene:
    def __init__(self, screen, surface):
        self.screen = screen
        self.surface = surface

    def enter(self):
        pass

    def exit(self):
        pass

    def if_exit(self):
        # 判断是否exit
        pass

    def render(self):
        pass


class ScenePrepare(Scene):
    def __init__(self, screen, surface):
        super().__init__(screen, surface)

    def enter(self):
        pass

    def exit(self):
        pass

    def if_exit(self):
        # 判断是否exit
        pass

    def render(self):
        pass
